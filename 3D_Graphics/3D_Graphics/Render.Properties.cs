﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Termpaper_3D_Graphics
{
    partial class Render
    {
        public bool UseZBuffer { get; set; }
        
        public bool DrawZBuffer { get; set; }

        public bool DrawLight { get; set; }

        public bool UseFlatLightning { get; set; }

        public Color WireColor { get; set; }

        public Color BackColor { get; set; }

        public Color LightColor { get; set; }                       

        public bool EyeByDirection { get; set; }

        public Vector3 EyePosition { get; set; }

        public Vector3 EyeDirection { get; set; }

        private Vector3 Eye { get; set; }

        public Vector3 LightPosition { get; set; }

        public double AmbientIntensy { get; set; }

        public double DiffuseIntensy { get; set; }

        public double SpecularIntensy { get; set; }        

        public double RenderTime { get; set; }

        public double BufferTime { get; set; }

        public Matrix4x4 ModelTransformation
        {
            get
            {
                return this.modelTransform;
            }
        }

        public Matrix4x4 ViewTransformation
        {
            get
            {
                return this.viewTransform;
            }
        }

        public Matrix4x4 ProjectionTransformation
        {
            get
            {
                return this.projectionTransform;
            }
        }
    }
}
