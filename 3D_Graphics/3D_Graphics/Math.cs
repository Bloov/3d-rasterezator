﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Termpaper_3D_Graphics
{
    class Matrix2x2
    {
        internal double[,] values;

        public Matrix2x2(bool identity = false)
        {
            values = new double[2, 2];
            for (int i = 0; i < 2; ++i)
                for (int j = 0; j < 2; ++j)
                    if ((i == j) && identity)
                        values[i, j] = 1;
                    else
                        values[i, j] = 0;
        }

        public Matrix2x2(Matrix2x2 source)
        {
            values = new double[2, 2];
            for (int i = 0; i < 2; ++i)
                for (int j = 0; j < 2; ++j)
                    values[i, j] = source.values[i, j];
        }

        public Matrix2x2(Matrix3x3 source, int skipRow, int skipCol)
        {
            values = new double[2, 2];
            int row = 0, col = 0;
            for (int i = 0; i < 3; ++i)
            {
                col = 0;
                for (int j = 0; j < 3; ++j)
                    if ((i != skipRow) && (j != skipCol))
                    {
                        values[row, col] = source.values[i, j];
                        ++col;
                    }

                if (i != skipRow)
                    row++;
            }
        }

        public Matrix2x2 Copy()
        {
            return new Matrix2x2(this);
        }

        private bool ValidIndex(int row, int col)
        {
            return (row >= 0) && (row < 2) &&
                   (col >= 0) && (col < 2);
        }

        public double this[int row, int col]
        {
            get
            {
                if (ValidIndex(row, col))
                    return values[row, col];
                else
                    return 0;
            }
            set
            {
                if (ValidIndex(row, col))
                    values[row, col] = value;
            }
        }
        
        public double Determinant()
        {
            return values[0, 0] * values[1, 1] - values[0, 1] * values[1, 0];
        }
    }

    class Matrix3x3
    {
        internal double[,] values;

        public Matrix3x3(bool identity = false)
        {
            values = new double[3, 3];
            for (int i = 0; i < 3; ++i)
                for (int j = 0; j < 3; ++j)
                    if ((i == j) && identity)
                        values[i, j] = 1;
                    else
                        values[i, j] = 0;
        }

        public Matrix3x3(Matrix3x3 source)
        {
            values = new double[3, 3];
            for (int i = 0; i < 3; ++i)
                for (int j = 0; j < 3; ++j)
                    values[i, j] = source.values[i, j];
        }

        public Matrix3x3(Matrix4x4 source)
        {
            values = new double[3, 3];
            for (int i = 0; i < 3; ++i)
                for (int j = 0; j < 3; ++j)
                    values[i, j] = source.values[i, j];
        }

        public Matrix3x3(Matrix4x4 source, int skipRow, int skipCol)
        {
            values = new double[3, 3];
            int row = 0, col = 0;
            for (int i = 0; i < 4; ++i)
            {
                col = 0;
                for (int j = 0; j < 4; ++j)
                    if ((i != skipRow) && (j != skipCol))
                    {
                        values[row, col] = source.values[i, j];
                        ++col;
                    }

                if (i != skipRow)
                    row++;
            }
        }

        public Matrix3x3 Copy()
        {
            return new Matrix3x3(this);
        }

        private bool ValidIndex(int row, int col)
        {
            return (row >= 0) && (row < 3) &&
                   (col >= 0) && (col < 3);
        }

        public double this[int row, int col]
        {
            get
            {
                if (ValidIndex(row, col))
                    return values[row, col];
                else
                    return 0;
            }
            set
            {
                if (ValidIndex(row, col))
                    values[row, col] = value;
            }
        }

        public Matrix3x3 Multiply(Matrix3x3 other)
        {
            var result = new Matrix3x3();
            for (int i = 0; i < 3; ++i)
                for (int j = 0; j < 3; ++j)
                {
                    result.values[i, j] = values[i, 0] * other.values[0, j] +
                                   values[i, 1] * other.values[1, j] +
                                   values[i, 2] * other.values[2, j];
                }
            return result;
        }

        public Matrix3x3 Transpose()
        {
            var result = new Matrix3x3();
            for (int i = 0; i < 3; ++i)
                for (int j = 0; j < 3; ++j)
                    result.values[i, j] = values[j, i];
            return result;
        }

        public double Determinant()
        {
            return values[0, 0] * values[1, 1] * values[2, 2] +
                   values[0, 1] * values[1, 2] * values[2, 0] +
                   values[0, 2] * values[1, 0] * values[2, 1] -
                   values[0, 0] * values[1, 2] * values[2, 1] -
                   values[0, 1] * values[1, 0] * values[2, 2] -
                   values[0, 2] * values[1, 1] * values[2, 0];
        }

        public Matrix3x3 Inverse()
        {
            var result = new Matrix3x3();
            var det = this.Determinant();
            Parallel.For(0, 3, row =>
            {
                for (int col = 0; col < 3; ++col)
                {
                    var sign = Utils.Pow(-1, row + col);
                    result.values[row, col] = new Matrix2x2(this, row, col).Determinant() * sign / det;
                }
            });
            return result;
        }
    }

    class Matrix4x4
    {
        internal double[,] values;

        public Matrix4x4(bool identity = false)
        {
            values = new double[4, 4];
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    if ((i == j) && identity)
                        values[i, j] = 1;
                    else
                        values[i, j] = 0;      
        }

        public Matrix4x4(Matrix4x4 source)
        {
            values = new double[4, 4];
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    values[i, j] = source.values[i, j];     
        }

        public Matrix4x4(Matrix3x3 source)
        {
            values = new double[4, 4];
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    if ((i < 3) && (j < 3))
                        values[i, j] = source.values[i, j];
                    else
                        if (i == j)
                            values[i, j] = 1;
                        else
                            values[i, j] = 0;
        }

        public Matrix4x4 Copy()
        {
            return new Matrix4x4(this);
        }

        private bool ValidIndex(int row, int col)
        {
            return (row >= 0) && (row < 4) &&
                   (col >= 0) && (col < 4);
        }

        public double this[int row, int col]
        {
            get 
            {
                if (ValidIndex(row, col))
                    return values[row, col];
                else
                    return 0;
            }
            set
            {
                if (ValidIndex(row, col))
                    values[row, col] = value;
            }
        }

        public Matrix4x4 Multiply(Matrix4x4 other)
        {
            var result = new Matrix4x4();
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                {
                    result.values[i, j] = values[i, 0] * other.values[0, j] +
                                          values[i, 1] * other.values[1, j] +
                                          values[i, 2] * other.values[2, j] +
                                          values[i, 3] * other.values[3, j];
                }
            return result;   
        }

        public double Determinant()
        {
            var result = 0.0;
            for (int i = 0; i < 4; ++i)
            {
                result += values[0, i] * new Matrix3x3(this, 1, i).Determinant();
            }
            return result;
        }

        public Matrix4x4 Transpose()
        {
            var result = new Matrix4x4();
            for (int i = 0; i < 4; ++i)
                for (int j = 0; j < 4; ++j)
                    result.values[i, j] = values[j, i];
            return result;
        }

        public Matrix4x4 Inverse()
        {
            var result = new Matrix4x4();
            var det = this.Determinant();
            Parallel.For(0, 4, row =>
            {
                for (int col = 0; col < 4; ++col)
                {
                    var sign = Utils.Pow(-1, row + col);
                    result.values[row, col] = new Matrix3x3(this, row, col).Determinant() * sign / det;
                }
            });
            return result;
        }
    }

    class Vector2
    {
        internal double x, y;

        public Vector2(double _x, double _y)
        {
            x = _x;
            y = _y;
        }


        public double X
        {
            get { return x; }
            set { x = value; }
        }

        public double Y
        {
            get { return y; }
            set { y = value; }
        }
    }

    struct Vector3
    {
        internal double x, y, z;

        public Vector3(double _x, double _y, double _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }

        public Vector3(Vector3 source)
        {
            x = source.X;
            y = source.Y;
            z = source.Z;
        }

        public Vector3(Vector4 source)
        {
            double W = source.w;
            x = source.x / W;
            y = source.y / W;
            z = source.z / W;
        }

        public static Vector3 operator +(Vector3 a, Vector3 b)
        {            
            a.x += b.x;
            a.y += b.y;
            a.z += b.z;
            return a;
        }

        public static Vector3 operator -(Vector3 a, Vector3 b)
        {            
            a.x -= b.x;
            a.y -= b.y;
            a.z -= b.z;
            return a;
        }

        public static Vector3 operator *(Vector3 a, double b)
        {            
            a.x *= b;
            a.y *= b;
            a.z *= b;
            return a;
        }

        public static Vector3 operator *(double a, Vector3 b)
        {
            b.x *= a;
            b.y *= a;
            b.z *= a;
            return b;
        }

        public double Length
        {
            get
            {
                return Math.Sqrt(x * x + y * y + z * z);
            }
        }

        public Vector3 Normalize()
        {
            var len = Math.Sqrt(x * x + y * y + z * z);
            x /= len;
            y /= len;
            z /= len;
            return this;
        }

        public Vector3 Normalized()
        {
            var result = this;
            var len = Length;
            result.x /= len;
            result.y /= len;
            result.z /= len;
            return result;
        }

        public double DotProduct(Vector3 other)
        {
            return x * other.x + y * other.y + z * other.z;
        }

        public Vector3 CrossProduct(Vector3 other)
        {
            Vector3 result = this;
            result.x = y * other.z - z * other.y;
            result.y = z * other.x - x * other.z;
            result.z = x * other.y - y * other.x;
            return result;
        }

        public Vector3 LinearInterpolate(Vector3 value, double progress)
        {
            var result = this;
            result.X += (value.x - x) * progress;
            result.Y += (value.y - y) * progress;
            result.Z += (value.z - z) * progress;
            return result;
        }

        public Vector3 Multiply(Matrix3x3 m)
        {
            Vector3 result = this;

            result.x = x * m.values[0, 0] + y * m.values[1, 0] + z * m.values[2, 0];
            result.y = x * m.values[0, 1] + y * m.values[1, 1] + z * m.values[2, 1];
            result.z = x * m.values[0, 2] + y * m.values[1, 2] + z * m.values[2, 2];

            return result;
        }        

        public Vector3 Reflect(Vector3 n)
        {
            var len = n.Length;
            var factor = (2 * this).DotProduct(n) / (len * len);
            return this - n * factor;
        }

        public double X 
        {
            get { return x; }
            set { x = value; }
        }

        public double Y
        {
            get { return y; }
            set { y = value; }
        }

        public double Z
        {
            get { return z; }
            set { z = value; }
        }        
    }

    struct Vector4
    {
        internal double x, y, z, w;

        public Vector4(double _x, double _y, double _z)
        {
            x = _x;
            y = _y;
            z = _z;
            w = 1.0;
        }

        public Vector4(Vector3 source)
        {
            x = source.x;
            y = source.y;
            z = source.z;
            w = 1.0;
        }

        public Vector4(Vector4 source)
        {
            x = source.x;
            y = source.y;
            z = source.z;
            w = source.w;
        }

        public static Vector4 operator +(Vector4 a, Vector4 b)
        {
            a.x += b.x;
            a.y += b.y;
            a.z += b.z;            
            return a;
        }

        public static Vector4 operator -(Vector4 a, Vector4 b)
        {
            a.x -= b.x;
            a.y -= b.y;
            a.z -= b.z;
            return a;
        }

        public Vector4 Multiply(Matrix4x4 m)
        {
            Vector4 result = this;
            result.x = x * m.values[0, 0] + y * m.values[1, 0] + z * m.values[2, 0] + w * m.values[3, 0];
            result.y = x * m.values[0, 1] + y * m.values[1, 1] + z * m.values[2, 1] + w * m.values[3, 1];
            result.z = x * m.values[0, 2] + y * m.values[1, 2] + z * m.values[2, 2] + w * m.values[3, 2];
            result.w = x * m.values[0, 3] + y * m.values[1, 3] + z * m.values[2, 3] + w * m.values[3, 3];            
            return result;
        }

        public Vector4 Normalize()
        {
            var result = this;
            result.x /= w;
            result.y /= w;
            result.z /= w;
            result.w = 1.0;
            return result;
        }        

        public Vector4 LinearInterpolate(Vector4 value, double progress)
        {
            var result = this;
            result.x += (value.x - x) * progress;
            result.y += (value.y - y) * progress;
            result.z += (value.z - z) * progress;
            result.w = 1.0;
            return result;
        }

        public double X
        {
            get { return x; }
            set { x = value; }
        }
       
        public double Y
        {
            get { return y; }
            set { y = value; }
        }

        public double Z
        {
            get { return z; }
            set { z = value; }
        }

        public double W
        {
            get { return w; }
            set { w = value; }
        }
    }

    static class Transformation
    {
        public static Matrix4x4 Identity()
        {
            return new Matrix4x4(true);
        }

        public static Matrix4x4 Shift(Vector3 shift)
        {
            var result = Identity();
            result[3, 0] = shift.X;
            result[3, 1] = shift.Y;
            result[3, 2] = shift.Z;
            return result;
        }

        public static Matrix4x4 Scale(Vector3 scale)
        {
            var result = Identity();
            result[0, 0] = scale.X;
            result[1, 1] = scale.Y;
            result[2, 2] = scale.Z;
            return result;
        }

        public static Matrix4x4 RotateX(double angle)
        {
            var result = Identity();
            double cosa = Math.Cos(angle);
            double sina = Math.Sin(angle);
            result[1, 1] = cosa;
            result[1, 2] = sina;
            result[2, 1] = -sina;
            result[2, 2] = cosa;
            return result;
        }

        public static Matrix4x4 RotateY(double angle)
        {
            var result = Identity();
            double cosa = Math.Cos(angle);
            double sina = Math.Sin(angle);
            result[0, 0] = cosa;
            result[2, 0] = sina;
            result[0, 2] = -sina;
            result[2, 2] = cosa;
            return result;
        }

        public static Matrix4x4 RotateZ(double angle)
        {
            var result = Identity();
            double cosa = Math.Cos(angle);
            double sina = Math.Sin(angle);
            result[0, 0] = cosa;
            result[0, 1] = sina;
            result[1, 0] = -sina;
            result[1, 1] = cosa;
            return result;
        }
    }

    static class ViewTransformation
    {
        public static Matrix4x4 Frontal()
        {
            return Transformation.Identity();
        }

        public static Matrix4x4 Profile()
        {
            return Transformation.Identity();
        }

        public static Matrix4x4 Horizontal()
        {
            return Transformation.Identity();
        }

        public static Matrix4x4 ExchangeXY()
        {
            var result = Transformation.Identity();
            result[0, 0] = 0;
            result[1, 1] = 0;
            result[1, 0] = 1;
            result[0, 1] = 1;
            return result;
        }

        public static Matrix4x4 ExchangeXZ()
        {
            var result = Transformation.Identity();
            result[0, 0] = 0;
            result[2, 2] = 0;
            result[2, 0] = 1;
            result[0, 2] = 1;
            return result;
        }

        public static Matrix4x4 ExchangeYZ()
        {
            var result = Transformation.Identity();
            result[1, 1] = 0;
            result[2, 2] = 0;
            result[1, 2] = 1;
            result[2, 1] = 1;
            return result;
        }

        public static Matrix4x4 Oblique(double l, double angle)
        {
            var result = Transformation.Identity();            
            result[2, 0] = l * Math.Cos(angle);
            result[2, 1] = l * Math.Sin(angle);
            return result;
        }

        public static Matrix4x4 Axonometric(double phi, double ksi)
        {
            var result = Transformation.Identity();
            double cosPhi = Math.Cos(phi);
            double sinPhi = Math.Sin(phi);
            double cosKsi = Math.Cos(ksi);
            double sinKsi = Math.Sin(ksi);
            result[0, 0] = cosKsi;
            result[0, 1] = sinPhi * sinKsi;
            result[1, 1] = cosPhi;
            result[2, 0] = sinKsi;
            result[2, 1] = -sinPhi * cosKsi;
            //result[2, 2] = 0;
            return result;
        }

        public static Matrix4x4 Perspective(double d)
        {
            var result = Transformation.Identity();
            result[2, 3] = 1 / d;
            result[3, 3] = 0;
            return result;
        }

        public static Matrix4x4 Frustum(double fovy, double aspect, double zFar, double zNear)
        {
            var result = Transformation.Identity();
            var f = Math.Cos(fovy / 2.0) / Math.Sin(fovy / 2.0);
            result[0, 0] = f / aspect;
            result[1, 1] = f;
            result[2, 2] = zFar / (zNear - zFar);
            result[3, 2] = -zFar * zNear / (zNear - zFar);
            result[2, 3] = 1;
            result[3, 3] = 0;
            
            return result;
        }

        public static Matrix4x4 ViewTransform(double ro, double phi, double theta)
        {
            var result = Transformation.Identity();
            double cosPhi = Math.Cos(phi);
            double sinPhi = Math.Sin(phi);
            double cosTheta = Math.Cos(theta);
            double sinTheta = Math.Sin(theta);
            result[0, 0] = -sinTheta;
            result[0, 1] = -cosPhi * cosTheta;
            result[0, 2] = -sinPhi * cosTheta;
            result[1, 0] = cosTheta;
            result[1, 1] = -cosPhi * sinTheta;
            result[1, 2] = -sinPhi * sinTheta;
            result[2, 1] = sinPhi;
            result[2, 2] = -cosPhi;
            result[3, 2] = ro;
            return result;
        }

        public static Matrix4x4 LookAt(Vector3 Eye, Vector3 At, Vector3 Up)
        {
            var result = new Matrix4x4(false);
            Vector3 vector = (At - Eye).Normalize();
            Vector3 right = Up.CrossProduct(vector).Normalize();
            Vector3 vector2 = vector.CrossProduct(right);            
            
            result[0, 0] = right.X;
            result[1, 0] = right.Y;
            result[2, 0] = right.Z;
            
            result[0, 1] = vector2.X;
            result[1, 1] = vector2.Y;
            result[2, 1] = vector2.Z;
            
            result[0, 2] = vector.X;
            result[1, 2] = vector.Y;
            result[2, 2] = vector.Z;
            
            result[3, 0] = -right.DotProduct(Eye);
            result[3, 1] = -vector2.DotProduct(Eye);
            result[3, 2] = -vector.DotProduct(Eye);
            result[3, 3] = 1.0;

            result[0, 3] = 0.0;
            result[1, 3] = 0.0;
            result[2, 3] = 0.0;

            return result;
        }

        public static Matrix4x4 BuildViewMatrix(Vector3 cameraTarget, double cameraRotationX, double cameraRotationY, double cameraTargetDistance)
        {
            var cameraRot = Transformation.RotateX(cameraRotationX).Multiply(Transformation.RotateY(cameraRotationY));
            var cameraOriginalTarget = new Vector4(0, 0, -1);
            var cameraOriginalUpVector = new Vector4(0, 1, 0);
            var cameraRotatedTarget = cameraOriginalTarget.Multiply(cameraRot);
            var cameraFinalTarget = new Vector4(cameraTarget) + cameraRotatedTarget;
            var cameraRotatedUpVector = cameraOriginalUpVector.Multiply(cameraRot);            
            var viewMatrix = LookAt(new Vector3(cameraTarget), new Vector3(cameraFinalTarget), new Vector3(cameraRotatedUpVector));
            return viewMatrix.Multiply(Transformation.Shift(new Vector3(0, 0, cameraTargetDistance)));
        }
    }
}
