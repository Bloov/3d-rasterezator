﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Termpaper_3D_Graphics
{
    partial class Render
    {
        private int width, height;
        private int halfWidth, halfHeight;                
        private Bitmap[] renderTargets;
        private int currentTarget;
        
        private ImageBuffer colorBuffer;
        private ZBuffer zBuffer;
        
        private Model light;

        private Matrix4x4 modelTransform;
        private Matrix4x4 viewTransform;
        private Matrix4x4 projectionTransform;

        private Vector3 lightPosition;
        private Matrix3x3 normalToModelViewSpaceTransform;
        private Matrix4x4 normalToScreenSpaceTransform;
        private Matrix4x4 modelViewTransform;
        
        public Render(Color backColor, Color wireColor)
        {
            UseZBuffer = true;
            DrawZBuffer = false;
            DrawLight = false;
            UseFlatLightning = false;

            WireColor = wireColor; 
            BackColor = backColor;
            LightColor = Color.White;

            EyeByDirection = true;
            EyeDirection = new Vector3(0, 0, 1);
            EyePosition = new Vector3(0, 0, 0);

            LightPosition = new Vector3(1000, 1000, 1000);            
            
            AmbientIntensy = 0.2;
            DiffuseIntensy = 0.7;
            SpecularIntensy = 1.2;            

            RenderTime = 0;
            BufferTime = 0;

            light = ModelMaker.MakeBox(4);
            for (int face = 0; face < light.FacesCount; ++face)
                light.GetFace(face).FaceColor = Color.White;

            renderTargets = new Bitmap[2] { null, null };
            currentTarget = 0;
        }       
        
        public void SetRenderTargetSize(int width, int height)
        {
            renderTargets[0] = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            renderTargets[1] = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            currentTarget = 0;           

            this.width = width;
            this.height = height;
            halfWidth = width / 2;
            halfHeight = height / 2;

            colorBuffer = new ImageBuffer(width, height, BackColor);
            zBuffer = new ZBuffer(width, height);                        
        }

        public Bitmap GetRenderTarget()
        {
            return renderTargets[currentTarget];
        }

        public void SetModelTransform(Matrix4x4 transform)
        {
            modelTransform = transform;           
        }

        public void SetViewTransform(Matrix4x4 transform)
        {
            viewTransform = transform;            
        }

        public void SetProjectionTransform(Matrix4x4 transform)
        {
            projectionTransform = transform;
        }

        public Matrix3x3 GetNormalToModelViewSpaceTransform()
        {
            var modelTr = new Matrix3x3(modelTransform);
            var viewTr = new Matrix3x3(viewTransform);
            return modelTr.Multiply(viewTr).Inverse();//.Transpose();
        }

        public Matrix4x4 GetNormalToScreenSpaceTransform()
        {
            var modelTr = new Matrix3x3(modelTransform);
            var viewTr = new Matrix3x3(viewTransform);
            var modelViewTr = new Matrix4x4(modelTr.Multiply(viewTr));
            return modelViewTr.Multiply(projectionTransform).Inverse().Transpose();                                
        }

        public Matrix4x4 GetModelViewTransform()
        {
            return modelTransform.Multiply(viewTransform);
        }

        public void DoWireRender(Model model)
        {
            Stopwatch profiler = new Stopwatch();
            profiler.Reset();
            profiler.Start();
                currentTarget = 1 - currentTarget;
                var render = Graphics.FromImage(renderTargets[currentTarget]);

                if (model == null)
                    return;

                var transform = modelTransform.Multiply(viewTransform).Multiply(projectionTransform);
                render.FillRectangle(new SolidBrush(BackColor), 0, 0, width, height);            
                for (int i = 0;  i < model.EdgesCount; ++i)
                {
                    var edge = model.GetEdge(i);                     

                    var rPointA = new Vector3(new Vector4(model.GetVertexUnsafe(edge.Start)).Multiply(transform).Normalize());                
                    var rPointB = new Vector3(new Vector4(model.GetVertexUnsafe(edge.End)).Multiply(transform).Normalize());
                    try
                    {
                        render.DrawLine(new Pen(WireColor),
                            width * 0.5f + (float)rPointA.X, height * 0.5f - (float)rPointA.Y,
                            width * 0.5f + (float)rPointB.X, height * 0.5f - (float)rPointB.Y);
                    }
                    finally { /*do nothing*/ }                
                }
            profiler.Stop();
            RenderTime = profiler.ElapsedMilliseconds;
            BufferTime = 0;            
        }

        public void DoFaceRender(Model model)
        {
            Stopwatch profiler = new Stopwatch();
            profiler.Reset();
            profiler.Start();
                currentTarget = 1 - currentTarget;
                colorBuffer.Clear(BackColor);
                zBuffer.Clear(double.MinValue);

                modelViewTransform = GetModelViewTransform();
                normalToModelViewSpaceTransform = GetNormalToModelViewSpaceTransform();
                normalToScreenSpaceTransform = GetNormalToScreenSpaceTransform();

                lightPosition = new Vector3(new Vector4(LightPosition).Multiply(viewTransform));

                DrawLigthSource();

                for (int face = 0; face < model.FacesCount; ++face)
                    DrawFace(model.GetFace(face));                                    

                if (DrawZBuffer)
                    CopyZBuffer2ColorBuffer();
            profiler.Stop();
            RenderTime = profiler.ElapsedMilliseconds;

            Stopwatch bufferProfiler = new Stopwatch();
            profiler.Reset();
            profiler.Start();
                colorBuffer.CopyToBitmap(renderTargets[currentTarget]);
            profiler.Stop();
            BufferTime = profiler.ElapsedMilliseconds;            
        }

        private void DrawLigthSource()
        {
            if (DrawLight)
            {
                DrawLight = false;
                var oldModelViewTransform = modelViewTransform;
                modelViewTransform = Transformation.Shift(LightPosition).Multiply(viewTransform);                

                for (int face = 0; face < light.FacesCount; ++face)
                    DrawFace(light.GetFace(face));

                DrawLight = true;
                modelViewTransform = oldModelViewTransform;
            }
        }

        private void CopyZBuffer2ColorBuffer()
        {
            var arrayOfMinZ = new double[height];
            var arrayOfMaxZ = new double[height];
            Parallel.For(0, height, y =>
            {
                double min = double.MaxValue;
                double max = double.MinValue;
                for (int x = 0; x < width; ++x)
                {
                    var z = zBuffer[x, y];
                    if (z > max)
                        max = z;
                    if ((z != double.MinValue) && (z < min))
                        min = z;
                }

                arrayOfMinZ[y] = min;
                arrayOfMaxZ[y] = max;
            });

            var minZ = arrayOfMinZ.Min();
            var maxZ = arrayOfMaxZ.Max();
            var deltaZ = maxZ - minZ;
            Parallel.For(0, height, y =>
            {
                for (int x = 0; x < width; ++x)
                {
                    var z = zBuffer[x, y];
                    if (z != double.MinValue)
                    {
                        int color;
                        if (maxZ != minZ)
                        {
                            var value = 220.0 * (z - minZ) / deltaZ;
                            color = 30 + Convert.ToInt32(value);
                        }
                        else
                            color = 250;
                        colorBuffer.SetColor(x, y, Color.FromArgb(255, color, color, color));
                    }
                    else
                        colorBuffer.SetColor(x, y, Color.Black);
                }
            });                         
        }

        private void DrawFace(Face face)
        {           
            Vector3 faceNormal, normal1, normal2, normal3;
            Vector4 modelPoint1, modelPoint2, modelPoint3;            
            Vector4 viewPoint1, viewPoint2, viewPoint3;

            faceNormal = face.Normal.Multiply(normalToModelViewSpaceTransform).Normalize();
            normal1 = face.GetVertexNormalUnsafe(0).Multiply(normalToModelViewSpaceTransform).Normalize();
            normal2 = face.GetVertexNormalUnsafe(1).Multiply(normalToModelViewSpaceTransform).Normalize();
            normal3 = face.GetVertexNormalUnsafe(2).Multiply(normalToModelViewSpaceTransform).Normalize();

            modelPoint1 = new Vector4(face.GetVertexUnsafe(0)).Multiply(modelViewTransform).Normalize();
            modelPoint2 = new Vector4(face.GetVertexUnsafe(1)).Multiply(modelViewTransform).Normalize();
            modelPoint3 = new Vector4(face.GetVertexUnsafe(2)).Multiply(modelViewTransform).Normalize();            

            var transform = modelViewTransform.Multiply(projectionTransform);
            viewPoint1 = new Vector4(face.GetVertexUnsafe(0)).Multiply(transform).Normalize();
            viewPoint2 = new Vector4(face.GetVertexUnsafe(1)).Multiply(transform).Normalize();
            viewPoint3 = new Vector4(face.GetVertexUnsafe(2)).Multiply(transform).Normalize();

            if (viewPoint1.Y < viewPoint2.Y)
            {
                Utils.Swap<Vector3>(ref normal1, ref normal2);
                Utils.Swap<Vector4>(ref modelPoint1, ref modelPoint2);
                Utils.Swap<Vector4>(ref viewPoint1, ref viewPoint2);
            }

            if (viewPoint1.Y < viewPoint3.Y)
            {
                Utils.Swap<Vector3>(ref normal1, ref normal3);
                Utils.Swap<Vector4>(ref modelPoint1, ref modelPoint3);
                Utils.Swap<Vector4>(ref viewPoint1, ref viewPoint3);
            }

            if (viewPoint2.Y < viewPoint3.Y)
            {
                Utils.Swap<Vector3>(ref normal2, ref normal3);
                Utils.Swap<Vector4>(ref modelPoint2, ref modelPoint3);
                Utils.Swap<Vector4>(ref viewPoint2, ref viewPoint3);
            }

            RasterizeTriangle(viewPoint1, viewPoint2, viewPoint3, 
                              modelPoint1, modelPoint2, modelPoint3,
                              normal1, normal2, normal3,
                              face, faceNormal);                    
        }
        
        private void PreparePointCoordinates(Vector4 point, out int x, out int y, out double z)
        {
            x = Utils.ClampDouble(point.X);
            y = Utils.ClampDouble(point.Y);
            z = point.Z;            
        }

        private void RasterizeTriangle(Vector4 V1, Vector4 V2, Vector4 V3,
                                       Vector4 M1, Vector4 M2, Vector4 M3,
                                       Vector3 N1, Vector3 N2, Vector3 N3, 
                                       Face face, Vector3 faceNormal)
        {
            //if (toEye.DotProduct(faceNormal) < 0)
            //    return;

            int x1, y1, x2, y2, x3, y3;
            double z1, z2, z3;
            PreparePointCoordinates(V1, out x1, out y1, out z1);
            PreparePointCoordinates(V2, out x2, out y2, out z2);
            PreparePointCoordinates(V3, out x3, out y3, out z3);
            
            double dy12 = y2 - y1;
            double dy13 = y3 - y1;
            double dy23 = y3 - y2;

            //for (int y = y1; y >= y2; --y)
            Parallel.For(y2, y1 + 1, y =>
            {
                int viewPortY = halfHeight - y;
                if ((viewPortY >= 0) && (viewPortY < height))
                {
                    int xa = x2, xb = x1;
                    double za = z2, zb = z1;
                    Vector4 mpa = M2, mpb = M1;
                    Vector3 na = N2, nb = N1;

                    if (y1 != y2)
                    {
                        double progressA = (y - y1) / dy12;
                        double progressB = (y - y1) / dy13;

                        xa = Utils.ClampDouble(x1 + (x2 - x1) * progressA);
                        xb = Utils.ClampDouble(x1 + (x3 - x1) * progressB);

                        za = z1 + (z2 - z1) * progressA;
                        zb = z1 + (z3 - z1) * progressB;

                        mpa = M1.LinearInterpolate(M2, progressA);
                        mpb = M1.LinearInterpolate(M3, progressB);

                        na = N1.LinearInterpolate(N2, progressA).Normalize();
                        nb = N1.LinearInterpolate(N3, progressB).Normalize();
                    }
                    RasterizeXLine(xa, xb, za, zb, y, viewPortY, mpa, mpb, na, nb, face, faceNormal);
                }
            });

            //for (int y = y2 - 1; y >= y3; --y)
            Parallel.For(y3, y2, y =>
            {
                int viewPortY = halfHeight - y;
                if ((viewPortY >= 0) && (viewPortY < height))
                {
                    int xa, xb;
                    double za, zb;
                    Vector4 mpa, mpb;
                    Vector3 na, nb;

                    double progressA = 1 - (y - y2) / dy23;
                    double progressB = (y - y1) / dy13;

                    xa = Utils.ClampDouble(x3 + (x2 - x3) * progressA);
                    xb = Utils.ClampDouble(x1 + (x3 - x1) * progressB);

                    za = z3 + (z2 - z3) * progressA;
                    zb = z1 + (z3 - z1) * progressB;

                    mpa = M3.LinearInterpolate(M2, progressA);
                    mpb = M1.LinearInterpolate(M3, progressB);

                    na = N3.LinearInterpolate(N2, progressA).Normalize();
                    nb = N1.LinearInterpolate(N3, progressB).Normalize();

                    RasterizeXLine(xa, xb, za, zb, y, viewPortY, mpa, mpb, na, nb, face, faceNormal);
                }
            });            
        }

        private void RasterizeXLine(int xa, int xb, double za, double zb, int y, int viewPortY, 
                                    Vector4 modelA, Vector4 modelB, 
                                    Vector3 normalA, Vector3 normalB, 
                                    Face face, Vector3 faceNormal)
        {
            if (xa > xb)
            {
                Utils.Swap<int>(ref xa, ref xb);
                Utils.Swap<double>(ref za, ref zb);
                Utils.Swap<Vector4>(ref modelA, ref modelB);
                Utils.Swap<Vector3>(ref normalA, ref normalB);
            }

            double dx = xb - xa;
            double dz = zb - za;

            for (int x = xa; x <= xb; ++x)
                if (xa != xb)                    
                {                    
                    int viewPortX = halfWidth + x;
                    if ((viewPortX >= 0) && (viewPortX < width))
                    {
                        var progress = (x - xa) / dx;
                        double z = za + dz * progress;
                        if (!UseZBuffer || (z > zBuffer[viewPortX, viewPortY]))
                        {
                            if (UseZBuffer)
                                zBuffer[viewPortX, viewPortY] = z;

                            var color = face.FaceColor;
                            if (DrawLight)
                            {
                                if (UseFlatLightning)
                                {
                                    var point = new Vector3(modelA.LinearInterpolate(modelB, progress));
                                    var toLight = (lightPosition - point).Normalize();

                                    var cosDiff = faceNormal.DotProduct(toLight);
                                    if (cosDiff < 0)
                                        cosDiff = 0;

                                    var diffuseComponent = DiffuseIntensy * face.DiffuseFactor * cosDiff;
                                    var intensy = AmbientIntensy + diffuseComponent;
                                    color = Utils.BlendLight(color, intensy, LightColor);
                                }
                                else
                                {
                                    var normal = normalA.LinearInterpolate(normalB, progress).Normalize();
                                    var point = new Vector3(modelA.LinearInterpolate(modelB, progress));
                                    var toLight = (lightPosition - point).Normalize();
                                    var toEye = EyeDirection;
                                    if (!EyeByDirection)
                                        toEye = EyePosition - point;
                                    toEye.Normalize();

                                    var cosSpec = 0.0;
                                    var cosDiff = normal.DotProduct(toLight);
                                    if (cosDiff >= 0)
                                    {
                                        var reflected = toLight.Reflect(normal);
                                        cosSpec = -toEye.DotProduct(reflected);                                        
                                        if (cosSpec < 0)
                                            cosSpec = 0;
                                    }
                                    else
                                        cosDiff = 0;                                    

                                    var diffuseComponent = DiffuseIntensy * face.DiffuseFactor * cosDiff;
                                    var specularComponent = SpecularIntensy * face.SpecularFactor * Math.Pow(cosSpec, face.SpecularShines);
                                    var intensy = AmbientIntensy + diffuseComponent + specularComponent;
                                    color = Utils.BlendLight(color, intensy, LightColor);

                                    /*var c = Utils.ClampDouble(20 + normal.DotProduct(toEye) * 220);
                                    var r = Utils.ClampDouble(128 + normal.X * 120);
                                    var g = Utils.ClampDouble(128 + normal.Y * 120);
                                    var b = Utils.ClampDouble(128 + normal.Z * 120);
                                    color = Color.FromArgb(255, r, g, b);
                                    color = Color.FromArgb(255, c, c, c);*/
                                }
                            }
                            else
                            {
                                var normal = normalA.LinearInterpolate(normalB, progress).Normalize();
                                var point = new Vector3(modelA.LinearInterpolate(modelB, progress));
                                var toEye = EyeDirection;
                                if (!EyeByDirection)
                                    toEye = EyePosition - point;
                                toEye.Normalize();

                                var cosDiff = normal.DotProduct(toEye);
                                if (cosDiff < 0)
                                    color = Utils.BlendLight(color, 0.6, color);
                            }

                            colorBuffer.SetColor(viewPortX, viewPortY, color);
                        }
                    }
                }
        }
    }
}
