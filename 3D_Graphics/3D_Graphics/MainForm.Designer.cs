﻿namespace Termpaper_3D_Graphics
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnWireColor = new System.Windows.Forms.Button();
            this.cbUseZBuffer = new System.Windows.Forms.CheckBox();
            this.btnBackColor = new System.Windows.Forms.Button();
            this.cbDrawZ = new System.Windows.Forms.CheckBox();
            this.btnRedrawFace = new System.Windows.Forms.Button();
            this.btnRedrawWire = new System.Windows.Forms.Button();
            this.pbRender = new System.Windows.Forms.PictureBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnLoad = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.lblRenderTime = new System.Windows.Forms.Label();
            this.lblBufferTime = new System.Windows.Forms.Label();
            this.lblHolePosition = new System.Windows.Forms.Label();
            this.lblTessValue = new System.Windows.Forms.Label();
            this.trbTessFactor = new System.Windows.Forms.TrackBar();
            this.label5 = new System.Windows.Forms.Label();
            this.tbModelSize = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.trbHolePosition = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.tbInnerRadius = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbOuterRadius = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnModel = new System.Windows.Forms.Button();
            this.btnBox = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnSZSub = new System.Windows.Forms.Button();
            this.f = new System.Windows.Forms.Button();
            this.btnSYSub = new System.Windows.Forms.Button();
            this.btnSYAdd = new System.Windows.Forms.Button();
            this.btnSXSub = new System.Windows.Forms.Button();
            this.btnSXAdd = new System.Windows.Forms.Button();
            this.btnDZSub = new System.Windows.Forms.Button();
            this.btnDZAdd = new System.Windows.Forms.Button();
            this.btnDYSub = new System.Windows.Forms.Button();
            this.btnDYAdd = new System.Windows.Forms.Button();
            this.btnDXSub = new System.Windows.Forms.Button();
            this.btnDXAdd = new System.Windows.Forms.Button();
            this.btnApplyTransform = new System.Windows.Forms.Button();
            this.btnRZSub = new System.Windows.Forms.Button();
            this.btnRZAdd = new System.Windows.Forms.Button();
            this.btnRYSub = new System.Windows.Forms.Button();
            this.btnRYAdd = new System.Windows.Forms.Button();
            this.btnRXSub = new System.Windows.Forms.Button();
            this.btnRXAdd = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.tbRZ = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbRY = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbRX = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbSZ = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbSY = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbSX = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbDZ = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbDY = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbDX = new System.Windows.Forms.TextBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.tbZFar = new System.Windows.Forms.TextBox();
            this.tbZNear = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbFovY = new System.Windows.Forms.TextBox();
            this.btnFrustum = new System.Windows.Forms.Button();
            this.btnVPRoSub = new System.Windows.Forms.Button();
            this.btnVPRoAdd = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbPerspectiveD = new System.Windows.Forms.TextBox();
            this.btnPerspective = new System.Windows.Forms.Button();
            this.tbAxonometrixKsi = new System.Windows.Forms.TextBox();
            this.tbAxonometrixPhi = new System.Windows.Forms.TextBox();
            this.btnAxonometric = new System.Windows.Forms.Button();
            this.tbObliqueAngle = new System.Windows.Forms.TextBox();
            this.tbObliqueL = new System.Windows.Forms.TextBox();
            this.btnOblique = new System.Windows.Forms.Button();
            this.btnVPPhSub = new System.Windows.Forms.Button();
            this.btnVPPhAdd = new System.Windows.Forms.Button();
            this.btnVPThSub = new System.Windows.Forms.Button();
            this.btnVPThAdd = new System.Windows.Forms.Button();
            this.tbVPTh = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tbVPPh = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbVPRo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbApplyVPT = new System.Windows.Forms.CheckBox();
            this.btnHorizontalView = new System.Windows.Forms.Button();
            this.btnProfileView = new System.Windows.Forms.Button();
            this.btnFrontalView = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cbUseFlatLightning = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.btnLightColor = new System.Windows.Forms.Button();
            this.tbModelSpecularShines = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.tbModelAmbient = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.tbModelSpecular = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.tbModelDiffuse = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.tbLightAmbient = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tbLightSpecular = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.tbLightDiffuse = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.btnApplyLight = new System.Windows.Forms.Button();
            this.btnResetLight = new System.Windows.Forms.Button();
            this.cbLight = new System.Windows.Forms.CheckBox();
            this.tbLightX = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbLightZ = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.tbLightY = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.openModelDialog = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRender)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbTessFactor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbHolePosition)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnWireColor);
            this.panel1.Controls.Add(this.cbUseZBuffer);
            this.panel1.Controls.Add(this.btnBackColor);
            this.panel1.Controls.Add(this.cbDrawZ);
            this.panel1.Controls.Add(this.btnRedrawFace);
            this.panel1.Controls.Add(this.btnRedrawWire);
            this.panel1.Controls.Add(this.pbRender);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(568, 534);
            this.panel1.TabIndex = 0;
            // 
            // btnWireColor
            // 
            this.btnWireColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWireColor.Location = new System.Drawing.Point(247, 3);
            this.btnWireColor.Name = "btnWireColor";
            this.btnWireColor.Size = new System.Drawing.Size(62, 27);
            this.btnWireColor.TabIndex = 18;
            this.btnWireColor.UseVisualStyleBackColor = true;
            this.btnWireColor.Click += new System.EventHandler(this.btnWireColor_Click);
            // 
            // cbUseZBuffer
            // 
            this.cbUseZBuffer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbUseZBuffer.AutoSize = true;
            this.cbUseZBuffer.Checked = true;
            this.cbUseZBuffer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbUseZBuffer.Location = new System.Drawing.Point(383, 9);
            this.cbUseZBuffer.Name = "cbUseZBuffer";
            this.cbUseZBuffer.Size = new System.Drawing.Size(85, 17);
            this.cbUseZBuffer.TabIndex = 17;
            this.cbUseZBuffer.Text = "Use Z buffer";
            this.cbUseZBuffer.UseVisualStyleBackColor = true;
            this.cbUseZBuffer.CheckedChanged += new System.EventHandler(this.cbUseZBuffer_CheckedChanged);
            // 
            // btnBackColor
            // 
            this.btnBackColor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBackColor.Location = new System.Drawing.Point(315, 3);
            this.btnBackColor.Name = "btnBackColor";
            this.btnBackColor.Size = new System.Drawing.Size(62, 27);
            this.btnBackColor.TabIndex = 16;
            this.btnBackColor.UseVisualStyleBackColor = true;
            this.btnBackColor.Click += new System.EventHandler(this.btnBackColor_Click);
            // 
            // cbDrawZ
            // 
            this.cbDrawZ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDrawZ.AutoSize = true;
            this.cbDrawZ.Location = new System.Drawing.Point(474, 9);
            this.cbDrawZ.Name = "cbDrawZ";
            this.cbDrawZ.Size = new System.Drawing.Size(91, 17);
            this.cbDrawZ.TabIndex = 15;
            this.cbDrawZ.Text = "Draw Z buffer";
            this.cbDrawZ.UseVisualStyleBackColor = true;
            this.cbDrawZ.CheckedChanged += new System.EventHandler(this.cbDrawZ_CheckedChanged);
            // 
            // btnRedrawFace
            // 
            this.btnRedrawFace.Location = new System.Drawing.Point(109, 3);
            this.btnRedrawFace.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.btnRedrawFace.Name = "btnRedrawFace";
            this.btnRedrawFace.Size = new System.Drawing.Size(105, 27);
            this.btnRedrawFace.TabIndex = 2;
            this.btnRedrawFace.Text = "Redraw face";
            this.btnRedrawFace.UseVisualStyleBackColor = true;
            this.btnRedrawFace.Click += new System.EventHandler(this.btnRedrawFace_Click);
            // 
            // btnRedrawWire
            // 
            this.btnRedrawWire.Location = new System.Drawing.Point(3, 3);
            this.btnRedrawWire.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.btnRedrawWire.Name = "btnRedrawWire";
            this.btnRedrawWire.Size = new System.Drawing.Size(100, 27);
            this.btnRedrawWire.TabIndex = 1;
            this.btnRedrawWire.Text = "Redraw wire";
            this.btnRedrawWire.UseVisualStyleBackColor = true;
            this.btnRedrawWire.Click += new System.EventHandler(this.btnRedrawWire_Click);
            // 
            // pbRender
            // 
            this.pbRender.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbRender.Location = new System.Drawing.Point(3, 33);
            this.pbRender.Name = "pbRender";
            this.pbRender.Size = new System.Drawing.Size(562, 498);
            this.pbRender.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbRender.TabIndex = 0;
            this.pbRender.TabStop = false;
            this.pbRender.ClientSizeChanged += new System.EventHandler(this.pbRender_ClientSizeChanged);
            this.pbRender.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbRender_MouseDown);
            this.pbRender.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbRender_MouseMove);
            this.pbRender.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbRender_MouseUp);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(586, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(226, 538);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnLoad);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.lblRenderTime);
            this.tabPage1.Controls.Add(this.lblBufferTime);
            this.tabPage1.Controls.Add(this.lblHolePosition);
            this.tabPage1.Controls.Add(this.lblTessValue);
            this.tabPage1.Controls.Add(this.trbTessFactor);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.tbModelSize);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.trbHolePosition);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.tbInnerRadius);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.tbOuterRadius);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btnModel);
            this.tabPage1.Controls.Add(this.btnBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(218, 512);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Model";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(138, 11);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(74, 23);
            this.btnLoad.TabIndex = 18;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(8, 276);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(205, 156);
            this.label38.TabIndex = 17;
            this.label38.Text = resources.GetString("label38.Text");
            // 
            // lblRenderTime
            // 
            this.lblRenderTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblRenderTime.AutoSize = true;
            this.lblRenderTime.Location = new System.Drawing.Point(6, 469);
            this.lblRenderTime.Margin = new System.Windows.Forms.Padding(3, 3, 3, 8);
            this.lblRenderTime.Name = "lblRenderTime";
            this.lblRenderTime.Size = new System.Drawing.Size(67, 13);
            this.lblRenderTime.TabIndex = 16;
            this.lblRenderTime.Text = "Render time:";
            // 
            // lblBufferTime
            // 
            this.lblBufferTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblBufferTime.AutoSize = true;
            this.lblBufferTime.Location = new System.Drawing.Point(6, 493);
            this.lblBufferTime.Margin = new System.Windows.Forms.Padding(3);
            this.lblBufferTime.Name = "lblBufferTime";
            this.lblBufferTime.Size = new System.Drawing.Size(60, 13);
            this.lblBufferTime.TabIndex = 15;
            this.lblBufferTime.Text = "Buffer time:";
            // 
            // lblHolePosition
            // 
            this.lblHolePosition.AutoSize = true;
            this.lblHolePosition.Location = new System.Drawing.Point(116, 132);
            this.lblHolePosition.Name = "lblHolePosition";
            this.lblHolePosition.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblHolePosition.Size = new System.Drawing.Size(13, 13);
            this.lblHolePosition.TabIndex = 14;
            this.lblHolePosition.Text = "0";
            this.lblHolePosition.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTessValue
            // 
            this.lblTessValue.AutoSize = true;
            this.lblTessValue.Location = new System.Drawing.Point(116, 198);
            this.lblTessValue.Name = "lblTessValue";
            this.lblTessValue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTessValue.Size = new System.Drawing.Size(13, 13);
            this.lblTessValue.TabIndex = 13;
            this.lblTessValue.Text = "1";
            this.lblTessValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // trbTessFactor
            // 
            this.trbTessFactor.Location = new System.Drawing.Point(6, 214);
            this.trbTessFactor.Margin = new System.Windows.Forms.Padding(3, 18, 3, 3);
            this.trbTessFactor.Minimum = 1;
            this.trbTessFactor.Name = "trbTessFactor";
            this.trbTessFactor.Size = new System.Drawing.Size(206, 45);
            this.trbTessFactor.TabIndex = 12;
            this.trbTessFactor.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trbTessFactor.Value = 1;
            this.trbTessFactor.ValueChanged += new System.EventHandler(this.trbTessFactor_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Tesselation factor";
            // 
            // tbModelSize
            // 
            this.tbModelSize.Location = new System.Drawing.Point(112, 53);
            this.tbModelSize.Margin = new System.Windows.Forms.Padding(3, 16, 3, 3);
            this.tbModelSize.Name = "tbModelSize";
            this.tbModelSize.Size = new System.Drawing.Size(100, 20);
            this.tbModelSize.TabIndex = 10;
            this.tbModelSize.Text = "360";
            this.tbModelSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Model size";
            // 
            // trbHolePosition
            // 
            this.trbHolePosition.Location = new System.Drawing.Point(6, 148);
            this.trbHolePosition.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
            this.trbHolePosition.Maximum = 25;
            this.trbHolePosition.Name = "trbHolePosition";
            this.trbHolePosition.Size = new System.Drawing.Size(206, 45);
            this.trbHolePosition.TabIndex = 8;
            this.trbHolePosition.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trbHolePosition.ValueChanged += new System.EventHandler(this.trbHolePosition_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Hole position (in %)";
            // 
            // tbInnerRadius
            // 
            this.tbInnerRadius.Location = new System.Drawing.Point(112, 105);
            this.tbInnerRadius.Name = "tbInnerRadius";
            this.tbInnerRadius.Size = new System.Drawing.Size(100, 20);
            this.tbInnerRadius.TabIndex = 6;
            this.tbInnerRadius.Text = "80";
            this.tbInnerRadius.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Inner radius";
            // 
            // tbOuterRadius
            // 
            this.tbOuterRadius.Location = new System.Drawing.Point(112, 79);
            this.tbOuterRadius.Name = "tbOuterRadius";
            this.tbOuterRadius.Size = new System.Drawing.Size(100, 20);
            this.tbOuterRadius.TabIndex = 4;
            this.tbOuterRadius.Text = "120";
            this.tbOuterRadius.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Outer radius";
            // 
            // btnModel
            // 
            this.btnModel.Location = new System.Drawing.Point(72, 11);
            this.btnModel.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.btnModel.Name = "btnModel";
            this.btnModel.Size = new System.Drawing.Size(60, 23);
            this.btnModel.TabIndex = 1;
            this.btnModel.Text = "Model";
            this.btnModel.UseVisualStyleBackColor = true;
            this.btnModel.Click += new System.EventHandler(this.btnModel_Click);
            // 
            // btnBox
            // 
            this.btnBox.Location = new System.Drawing.Point(6, 11);
            this.btnBox.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.btnBox.Name = "btnBox";
            this.btnBox.Size = new System.Drawing.Size(60, 23);
            this.btnBox.TabIndex = 0;
            this.btnBox.Text = "Box";
            this.btnBox.UseVisualStyleBackColor = true;
            this.btnBox.Click += new System.EventHandler(this.btnBox_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnSZSub);
            this.tabPage4.Controls.Add(this.f);
            this.tabPage4.Controls.Add(this.btnSYSub);
            this.tabPage4.Controls.Add(this.btnSYAdd);
            this.tabPage4.Controls.Add(this.btnSXSub);
            this.tabPage4.Controls.Add(this.btnSXAdd);
            this.tabPage4.Controls.Add(this.btnDZSub);
            this.tabPage4.Controls.Add(this.btnDZAdd);
            this.tabPage4.Controls.Add(this.btnDYSub);
            this.tabPage4.Controls.Add(this.btnDYAdd);
            this.tabPage4.Controls.Add(this.btnDXSub);
            this.tabPage4.Controls.Add(this.btnDXAdd);
            this.tabPage4.Controls.Add(this.btnApplyTransform);
            this.tabPage4.Controls.Add(this.btnRZSub);
            this.tabPage4.Controls.Add(this.btnRZAdd);
            this.tabPage4.Controls.Add(this.btnRYSub);
            this.tabPage4.Controls.Add(this.btnRYAdd);
            this.tabPage4.Controls.Add(this.btnRXSub);
            this.tabPage4.Controls.Add(this.btnRXAdd);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.tbRZ);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.tbRY);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.tbRX);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.tbSZ);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.tbSY);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.tbSX);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.tbDZ);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.tbDY);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.tbDX);
            this.tabPage4.Controls.Add(this.btnReset);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(218, 512);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Transform";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btnSZSub
            // 
            this.btnSZSub.Location = new System.Drawing.Point(35, 280);
            this.btnSZSub.Name = "btnSZSub";
            this.btnSZSub.Size = new System.Drawing.Size(20, 20);
            this.btnSZSub.TabIndex = 37;
            this.btnSZSub.Text = "-";
            this.btnSZSub.UseVisualStyleBackColor = true;
            this.btnSZSub.Click += new System.EventHandler(this.btnSZSub_Click);
            // 
            // f
            // 
            this.f.Location = new System.Drawing.Point(167, 280);
            this.f.Name = "f";
            this.f.Size = new System.Drawing.Size(20, 20);
            this.f.TabIndex = 36;
            this.f.Text = "+";
            this.f.UseVisualStyleBackColor = true;
            this.f.Click += new System.EventHandler(this.f_Click);
            // 
            // btnSYSub
            // 
            this.btnSYSub.Location = new System.Drawing.Point(35, 241);
            this.btnSYSub.Name = "btnSYSub";
            this.btnSYSub.Size = new System.Drawing.Size(20, 20);
            this.btnSYSub.TabIndex = 35;
            this.btnSYSub.Text = "-";
            this.btnSYSub.UseVisualStyleBackColor = true;
            this.btnSYSub.Click += new System.EventHandler(this.btnSYSub_Click);
            // 
            // btnSYAdd
            // 
            this.btnSYAdd.Location = new System.Drawing.Point(167, 241);
            this.btnSYAdd.Name = "btnSYAdd";
            this.btnSYAdd.Size = new System.Drawing.Size(20, 20);
            this.btnSYAdd.TabIndex = 34;
            this.btnSYAdd.Text = "+";
            this.btnSYAdd.UseVisualStyleBackColor = true;
            this.btnSYAdd.Click += new System.EventHandler(this.btnSYAdd_Click);
            // 
            // btnSXSub
            // 
            this.btnSXSub.Location = new System.Drawing.Point(35, 203);
            this.btnSXSub.Name = "btnSXSub";
            this.btnSXSub.Size = new System.Drawing.Size(20, 20);
            this.btnSXSub.TabIndex = 33;
            this.btnSXSub.Text = "-";
            this.btnSXSub.UseVisualStyleBackColor = true;
            this.btnSXSub.Click += new System.EventHandler(this.btnSXSub_Click);
            // 
            // btnSXAdd
            // 
            this.btnSXAdd.Location = new System.Drawing.Point(167, 203);
            this.btnSXAdd.Name = "btnSXAdd";
            this.btnSXAdd.Size = new System.Drawing.Size(20, 20);
            this.btnSXAdd.TabIndex = 32;
            this.btnSXAdd.Text = "+";
            this.btnSXAdd.UseVisualStyleBackColor = true;
            this.btnSXAdd.Click += new System.EventHandler(this.btnSXAdd_Click);
            // 
            // btnDZSub
            // 
            this.btnDZSub.Location = new System.Drawing.Point(35, 147);
            this.btnDZSub.Name = "btnDZSub";
            this.btnDZSub.Size = new System.Drawing.Size(20, 20);
            this.btnDZSub.TabIndex = 31;
            this.btnDZSub.Text = "-";
            this.btnDZSub.UseVisualStyleBackColor = true;
            this.btnDZSub.Click += new System.EventHandler(this.btnDZSub_Click);
            // 
            // btnDZAdd
            // 
            this.btnDZAdd.Location = new System.Drawing.Point(167, 147);
            this.btnDZAdd.Name = "btnDZAdd";
            this.btnDZAdd.Size = new System.Drawing.Size(20, 20);
            this.btnDZAdd.TabIndex = 30;
            this.btnDZAdd.Text = "+";
            this.btnDZAdd.UseVisualStyleBackColor = true;
            this.btnDZAdd.Click += new System.EventHandler(this.btnDZAdd_Click);
            // 
            // btnDYSub
            // 
            this.btnDYSub.Location = new System.Drawing.Point(35, 108);
            this.btnDYSub.Name = "btnDYSub";
            this.btnDYSub.Size = new System.Drawing.Size(20, 20);
            this.btnDYSub.TabIndex = 29;
            this.btnDYSub.Text = "-";
            this.btnDYSub.UseVisualStyleBackColor = true;
            this.btnDYSub.Click += new System.EventHandler(this.btnDYSub_Click);
            // 
            // btnDYAdd
            // 
            this.btnDYAdd.Location = new System.Drawing.Point(167, 108);
            this.btnDYAdd.Name = "btnDYAdd";
            this.btnDYAdd.Size = new System.Drawing.Size(20, 20);
            this.btnDYAdd.TabIndex = 28;
            this.btnDYAdd.Text = "+";
            this.btnDYAdd.UseVisualStyleBackColor = true;
            this.btnDYAdd.Click += new System.EventHandler(this.btnDYAdd_Click);
            // 
            // btnDXSub
            // 
            this.btnDXSub.Location = new System.Drawing.Point(35, 70);
            this.btnDXSub.Name = "btnDXSub";
            this.btnDXSub.Size = new System.Drawing.Size(20, 20);
            this.btnDXSub.TabIndex = 27;
            this.btnDXSub.Text = "-";
            this.btnDXSub.UseVisualStyleBackColor = true;
            this.btnDXSub.Click += new System.EventHandler(this.btnDXSub_Click);
            // 
            // btnDXAdd
            // 
            this.btnDXAdd.Location = new System.Drawing.Point(167, 70);
            this.btnDXAdd.Name = "btnDXAdd";
            this.btnDXAdd.Size = new System.Drawing.Size(20, 20);
            this.btnDXAdd.TabIndex = 26;
            this.btnDXAdd.Text = "+";
            this.btnDXAdd.UseVisualStyleBackColor = true;
            this.btnDXAdd.Click += new System.EventHandler(this.btnDXAdd_Click);
            // 
            // btnApplyTransform
            // 
            this.btnApplyTransform.Location = new System.Drawing.Point(8, 11);
            this.btnApplyTransform.Margin = new System.Windows.Forms.Padding(8, 3, 3, 3);
            this.btnApplyTransform.Name = "btnApplyTransform";
            this.btnApplyTransform.Size = new System.Drawing.Size(93, 24);
            this.btnApplyTransform.TabIndex = 25;
            this.btnApplyTransform.Text = "Apply";
            this.btnApplyTransform.UseVisualStyleBackColor = true;
            this.btnApplyTransform.Click += new System.EventHandler(this.btnApplyTransform_Click);
            // 
            // btnRZSub
            // 
            this.btnRZSub.Location = new System.Drawing.Point(35, 414);
            this.btnRZSub.Name = "btnRZSub";
            this.btnRZSub.Size = new System.Drawing.Size(20, 20);
            this.btnRZSub.TabIndex = 24;
            this.btnRZSub.Text = "-";
            this.btnRZSub.UseVisualStyleBackColor = true;
            this.btnRZSub.Click += new System.EventHandler(this.btnRZSub_Click);
            // 
            // btnRZAdd
            // 
            this.btnRZAdd.Location = new System.Drawing.Point(167, 413);
            this.btnRZAdd.Name = "btnRZAdd";
            this.btnRZAdd.Size = new System.Drawing.Size(20, 20);
            this.btnRZAdd.TabIndex = 23;
            this.btnRZAdd.Text = "+";
            this.btnRZAdd.UseVisualStyleBackColor = true;
            this.btnRZAdd.Click += new System.EventHandler(this.btnRZAdd_Click);
            // 
            // btnRYSub
            // 
            this.btnRYSub.Location = new System.Drawing.Point(35, 374);
            this.btnRYSub.Name = "btnRYSub";
            this.btnRYSub.Size = new System.Drawing.Size(20, 20);
            this.btnRYSub.TabIndex = 22;
            this.btnRYSub.Text = "-";
            this.btnRYSub.UseVisualStyleBackColor = true;
            this.btnRYSub.Click += new System.EventHandler(this.btnRYSub_Click);
            // 
            // btnRYAdd
            // 
            this.btnRYAdd.Location = new System.Drawing.Point(167, 374);
            this.btnRYAdd.Name = "btnRYAdd";
            this.btnRYAdd.Size = new System.Drawing.Size(20, 20);
            this.btnRYAdd.TabIndex = 21;
            this.btnRYAdd.Text = "+";
            this.btnRYAdd.UseVisualStyleBackColor = true;
            this.btnRYAdd.Click += new System.EventHandler(this.btnRYAdd_Click);
            // 
            // btnRXSub
            // 
            this.btnRXSub.Location = new System.Drawing.Point(35, 336);
            this.btnRXSub.Name = "btnRXSub";
            this.btnRXSub.Size = new System.Drawing.Size(20, 20);
            this.btnRXSub.TabIndex = 20;
            this.btnRXSub.Text = "-";
            this.btnRXSub.UseVisualStyleBackColor = true;
            this.btnRXSub.Click += new System.EventHandler(this.btnRXSub_Click);
            // 
            // btnRXAdd
            // 
            this.btnRXAdd.Location = new System.Drawing.Point(167, 336);
            this.btnRXAdd.Name = "btnRXAdd";
            this.btnRXAdd.Size = new System.Drawing.Size(20, 20);
            this.btnRXAdd.TabIndex = 19;
            this.btnRXAdd.Text = "+";
            this.btnRXAdd.UseVisualStyleBackColor = true;
            this.btnRXAdd.Click += new System.EventHandler(this.btnRXAdd_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(90, 398);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Rotate Z";
            // 
            // tbRZ
            // 
            this.tbRZ.Location = new System.Drawing.Point(61, 414);
            this.tbRZ.Name = "tbRZ";
            this.tbRZ.Size = new System.Drawing.Size(100, 20);
            this.tbRZ.TabIndex = 17;
            this.tbRZ.Text = "0";
            this.tbRZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(90, 359);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Rotate Y";
            // 
            // tbRY
            // 
            this.tbRY.Location = new System.Drawing.Point(61, 375);
            this.tbRY.Name = "tbRY";
            this.tbRY.Size = new System.Drawing.Size(100, 20);
            this.tbRY.TabIndex = 15;
            this.tbRY.Text = "0";
            this.tbRY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(90, 320);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 16, 3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Rotate X";
            // 
            // tbRX
            // 
            this.tbRX.Location = new System.Drawing.Point(61, 336);
            this.tbRX.Name = "tbRX";
            this.tbRX.Size = new System.Drawing.Size(100, 20);
            this.tbRX.TabIndex = 13;
            this.tbRX.Text = "0";
            this.tbRX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(90, 265);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Scale Z";
            // 
            // tbSZ
            // 
            this.tbSZ.Location = new System.Drawing.Point(61, 281);
            this.tbSZ.Name = "tbSZ";
            this.tbSZ.Size = new System.Drawing.Size(100, 20);
            this.tbSZ.TabIndex = 11;
            this.tbSZ.Text = "1";
            this.tbSZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(90, 226);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Scale Y";
            // 
            // tbSY
            // 
            this.tbSY.Location = new System.Drawing.Point(61, 242);
            this.tbSY.Name = "tbSY";
            this.tbSY.Size = new System.Drawing.Size(100, 20);
            this.tbSY.TabIndex = 9;
            this.tbSY.Text = "1";
            this.tbSY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(90, 187);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 16, 3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Scale X";
            // 
            // tbSX
            // 
            this.tbSX.Location = new System.Drawing.Point(61, 203);
            this.tbSX.Name = "tbSX";
            this.tbSX.Size = new System.Drawing.Size(100, 20);
            this.tbSX.TabIndex = 7;
            this.tbSX.Text = "1";
            this.tbSX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(90, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Shift Z";
            // 
            // tbDZ
            // 
            this.tbDZ.Location = new System.Drawing.Point(61, 148);
            this.tbDZ.Name = "tbDZ";
            this.tbDZ.Size = new System.Drawing.Size(100, 20);
            this.tbDZ.TabIndex = 5;
            this.tbDZ.Text = "0";
            this.tbDZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(90, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Shift Y";
            // 
            // tbDY
            // 
            this.tbDY.Location = new System.Drawing.Point(61, 109);
            this.tbDY.Name = "tbDY";
            this.tbDY.Size = new System.Drawing.Size(100, 20);
            this.tbDY.TabIndex = 3;
            this.tbDY.Text = "0";
            this.tbDY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(90, 54);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 16, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Shift X";
            // 
            // tbDX
            // 
            this.tbDX.Location = new System.Drawing.Point(61, 70);
            this.tbDX.Name = "tbDX";
            this.tbDX.Size = new System.Drawing.Size(100, 20);
            this.tbDX.TabIndex = 1;
            this.tbDX.Text = "0";
            this.tbDX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(117, 11);
            this.btnReset.Margin = new System.Windows.Forms.Padding(3, 3, 8, 3);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(93, 24);
            this.btnReset.TabIndex = 0;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.tbZFar);
            this.tabPage2.Controls.Add(this.tbZNear);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.tbFovY);
            this.tabPage2.Controls.Add(this.btnFrustum);
            this.tabPage2.Controls.Add(this.btnVPRoSub);
            this.tabPage2.Controls.Add(this.btnVPRoAdd);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.tbPerspectiveD);
            this.tabPage2.Controls.Add(this.btnPerspective);
            this.tabPage2.Controls.Add(this.tbAxonometrixKsi);
            this.tabPage2.Controls.Add(this.tbAxonometrixPhi);
            this.tabPage2.Controls.Add(this.btnAxonometric);
            this.tabPage2.Controls.Add(this.tbObliqueAngle);
            this.tabPage2.Controls.Add(this.tbObliqueL);
            this.tabPage2.Controls.Add(this.btnOblique);
            this.tabPage2.Controls.Add(this.btnVPPhSub);
            this.tabPage2.Controls.Add(this.btnVPPhAdd);
            this.tabPage2.Controls.Add(this.btnVPThSub);
            this.tabPage2.Controls.Add(this.btnVPThAdd);
            this.tabPage2.Controls.Add(this.tbVPTh);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.tbVPPh);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.tbVPRo);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.cbApplyVPT);
            this.tabPage2.Controls.Add(this.btnHorizontalView);
            this.tabPage2.Controls.Add(this.btnProfileView);
            this.tabPage2.Controls.Add(this.btnFrontalView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(218, 512);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "View";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(179, 242);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(22, 13);
            this.label24.TabIndex = 35;
            this.label24.Text = "Far";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(121, 242);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(30, 13);
            this.label25.TabIndex = 34;
            this.label25.Text = "Near";
            // 
            // tbZFar
            // 
            this.tbZFar.Location = new System.Drawing.Point(164, 261);
            this.tbZFar.Margin = new System.Windows.Forms.Padding(3, 24, 3, 3);
            this.tbZFar.Name = "tbZFar";
            this.tbZFar.Size = new System.Drawing.Size(51, 20);
            this.tbZFar.TabIndex = 33;
            this.tbZFar.Text = "-5000";
            this.tbZFar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbZNear
            // 
            this.tbZNear.Location = new System.Drawing.Point(107, 261);
            this.tbZNear.Name = "tbZNear";
            this.tbZNear.Size = new System.Drawing.Size(51, 20);
            this.tbZNear.TabIndex = 32;
            this.tbZNear.Text = "5000";
            this.tbZNear.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(167, 198);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(38, 13);
            this.label23.TabIndex = 31;
            this.label23.Text = "FOV Y";
            // 
            // tbFovY
            // 
            this.tbFovY.Location = new System.Drawing.Point(161, 214);
            this.tbFovY.Margin = new System.Windows.Forms.Padding(3, 24, 3, 3);
            this.tbFovY.Name = "tbFovY";
            this.tbFovY.Size = new System.Drawing.Size(51, 20);
            this.tbFovY.TabIndex = 30;
            this.tbFovY.Text = "0,1";
            this.tbFovY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnFrustum
            // 
            this.btnFrustum.Location = new System.Drawing.Point(6, 212);
            this.btnFrustum.Name = "btnFrustum";
            this.btnFrustum.Size = new System.Drawing.Size(92, 23);
            this.btnFrustum.TabIndex = 29;
            this.btnFrustum.Text = "Frustum";
            this.btnFrustum.UseVisualStyleBackColor = true;
            this.btnFrustum.Click += new System.EventHandler(this.btnFrustum_Click);
            // 
            // btnVPRoSub
            // 
            this.btnVPRoSub.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVPRoSub.Location = new System.Drawing.Point(15, 391);
            this.btnVPRoSub.Name = "btnVPRoSub";
            this.btnVPRoSub.Size = new System.Drawing.Size(34, 20);
            this.btnVPRoSub.TabIndex = 28;
            this.btnVPRoSub.Text = "-";
            this.btnVPRoSub.UseVisualStyleBackColor = true;
            this.btnVPRoSub.Click += new System.EventHandler(this.btnVPRoSub_Click);
            // 
            // btnVPRoAdd
            // 
            this.btnVPRoAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVPRoAdd.Location = new System.Drawing.Point(161, 393);
            this.btnVPRoAdd.Name = "btnVPRoAdd";
            this.btnVPRoAdd.Size = new System.Drawing.Size(34, 20);
            this.btnVPRoAdd.TabIndex = 27;
            this.btnVPRoAdd.Text = "+";
            this.btnVPRoAdd.UseVisualStyleBackColor = true;
            this.btnVPRoAdd.Click += new System.EventHandler(this.btnVPRoAdd_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(139, 151);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 13);
            this.label22.TabIndex = 26;
            this.label22.Text = "D scale factor";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(176, 101);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(21, 13);
            this.label21.TabIndex = 25;
            this.label21.Text = "Ksi";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(118, 101);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(22, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "Phi";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(171, 57);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(34, 13);
            this.label19.TabIndex = 23;
            this.label19.Text = "Angle";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(123, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "L";
            // 
            // tbPerspectiveD
            // 
            this.tbPerspectiveD.Location = new System.Drawing.Point(161, 167);
            this.tbPerspectiveD.Margin = new System.Windows.Forms.Padding(3, 24, 3, 3);
            this.tbPerspectiveD.Name = "tbPerspectiveD";
            this.tbPerspectiveD.Size = new System.Drawing.Size(51, 20);
            this.tbPerspectiveD.TabIndex = 21;
            this.tbPerspectiveD.Text = "800";
            this.tbPerspectiveD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnPerspective
            // 
            this.btnPerspective.Location = new System.Drawing.Point(6, 165);
            this.btnPerspective.Name = "btnPerspective";
            this.btnPerspective.Size = new System.Drawing.Size(92, 23);
            this.btnPerspective.TabIndex = 20;
            this.btnPerspective.Text = "Perspective";
            this.btnPerspective.UseVisualStyleBackColor = true;
            this.btnPerspective.Click += new System.EventHandler(this.btnPerspective_Click);
            // 
            // tbAxonometrixKsi
            // 
            this.tbAxonometrixKsi.Location = new System.Drawing.Point(161, 120);
            this.tbAxonometrixKsi.Margin = new System.Windows.Forms.Padding(3, 24, 3, 3);
            this.tbAxonometrixKsi.Name = "tbAxonometrixKsi";
            this.tbAxonometrixKsi.Size = new System.Drawing.Size(51, 20);
            this.tbAxonometrixKsi.TabIndex = 19;
            this.tbAxonometrixKsi.Text = "30";
            this.tbAxonometrixKsi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbAxonometrixPhi
            // 
            this.tbAxonometrixPhi.Location = new System.Drawing.Point(104, 120);
            this.tbAxonometrixPhi.Name = "tbAxonometrixPhi";
            this.tbAxonometrixPhi.Size = new System.Drawing.Size(51, 20);
            this.tbAxonometrixPhi.TabIndex = 18;
            this.tbAxonometrixPhi.Text = "45";
            this.tbAxonometrixPhi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnAxonometric
            // 
            this.btnAxonometric.Location = new System.Drawing.Point(6, 118);
            this.btnAxonometric.Name = "btnAxonometric";
            this.btnAxonometric.Size = new System.Drawing.Size(92, 23);
            this.btnAxonometric.TabIndex = 17;
            this.btnAxonometric.Text = "Axonometric";
            this.btnAxonometric.UseVisualStyleBackColor = true;
            this.btnAxonometric.Click += new System.EventHandler(this.btnAxonometric_Click);
            // 
            // tbObliqueAngle
            // 
            this.tbObliqueAngle.Location = new System.Drawing.Point(161, 73);
            this.tbObliqueAngle.Margin = new System.Windows.Forms.Padding(3, 36, 3, 3);
            this.tbObliqueAngle.Name = "tbObliqueAngle";
            this.tbObliqueAngle.Size = new System.Drawing.Size(51, 20);
            this.tbObliqueAngle.TabIndex = 16;
            this.tbObliqueAngle.Text = "20";
            this.tbObliqueAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbObliqueL
            // 
            this.tbObliqueL.Location = new System.Drawing.Point(104, 73);
            this.tbObliqueL.Name = "tbObliqueL";
            this.tbObliqueL.Size = new System.Drawing.Size(51, 20);
            this.tbObliqueL.TabIndex = 15;
            this.tbObliqueL.Text = "0.6";
            this.tbObliqueL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnOblique
            // 
            this.btnOblique.Location = new System.Drawing.Point(6, 71);
            this.btnOblique.Name = "btnOblique";
            this.btnOblique.Size = new System.Drawing.Size(92, 23);
            this.btnOblique.TabIndex = 14;
            this.btnOblique.Text = "Oblique";
            this.btnOblique.UseVisualStyleBackColor = true;
            this.btnOblique.Click += new System.EventHandler(this.btnOblique_Click);
            // 
            // btnVPPhSub
            // 
            this.btnVPPhSub.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVPPhSub.Location = new System.Drawing.Point(15, 485);
            this.btnVPPhSub.Name = "btnVPPhSub";
            this.btnVPPhSub.Size = new System.Drawing.Size(34, 20);
            this.btnVPPhSub.TabIndex = 13;
            this.btnVPPhSub.Text = "-";
            this.btnVPPhSub.UseVisualStyleBackColor = true;
            this.btnVPPhSub.Click += new System.EventHandler(this.btnVPPhSub_Click);
            // 
            // btnVPPhAdd
            // 
            this.btnVPPhAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVPPhAdd.Location = new System.Drawing.Point(161, 486);
            this.btnVPPhAdd.Name = "btnVPPhAdd";
            this.btnVPPhAdd.Size = new System.Drawing.Size(34, 20);
            this.btnVPPhAdd.TabIndex = 12;
            this.btnVPPhAdd.Text = "+";
            this.btnVPPhAdd.UseVisualStyleBackColor = true;
            this.btnVPPhAdd.Click += new System.EventHandler(this.btnVPPhAdd_Click);
            // 
            // btnVPThSub
            // 
            this.btnVPThSub.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVPThSub.Location = new System.Drawing.Point(15, 438);
            this.btnVPThSub.Name = "btnVPThSub";
            this.btnVPThSub.Size = new System.Drawing.Size(34, 20);
            this.btnVPThSub.TabIndex = 11;
            this.btnVPThSub.Text = "-";
            this.btnVPThSub.UseVisualStyleBackColor = true;
            this.btnVPThSub.Click += new System.EventHandler(this.btnVPThSub_Click);
            // 
            // btnVPThAdd
            // 
            this.btnVPThAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVPThAdd.Location = new System.Drawing.Point(161, 440);
            this.btnVPThAdd.Name = "btnVPThAdd";
            this.btnVPThAdd.Size = new System.Drawing.Size(34, 20);
            this.btnVPThAdd.TabIndex = 10;
            this.btnVPThAdd.Text = "+";
            this.btnVPThAdd.UseVisualStyleBackColor = true;
            this.btnVPThAdd.Click += new System.EventHandler(this.btnVPThAdd_Click);
            // 
            // tbVPTh
            // 
            this.tbVPTh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbVPTh.Location = new System.Drawing.Point(55, 440);
            this.tbVPTh.Name = "tbVPTh";
            this.tbVPTh.Size = new System.Drawing.Size(100, 20);
            this.tbVPTh.TabIndex = 9;
            this.tbVPTh.Text = "0";
            this.tbVPTh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(62, 424);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 13);
            this.label18.TabIndex = 8;
            this.label18.Text = "View point theta";
            // 
            // tbVPPh
            // 
            this.tbVPPh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbVPPh.Location = new System.Drawing.Point(55, 486);
            this.tbVPPh.Name = "tbVPPh";
            this.tbVPPh.Size = new System.Drawing.Size(100, 20);
            this.tbVPPh.TabIndex = 7;
            this.tbVPPh.Text = "0";
            this.tbVPPh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(72, 470);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(73, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "View point phi";
            // 
            // tbVPRo
            // 
            this.tbVPRo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbVPRo.Location = new System.Drawing.Point(55, 393);
            this.tbVPRo.Name = "tbVPRo";
            this.tbVPRo.Size = new System.Drawing.Size(100, 20);
            this.tbVPRo.TabIndex = 5;
            this.tbVPRo.Text = "1400";
            this.tbVPRo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(77, 377);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(68, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "View point ro";
            // 
            // cbApplyVPT
            // 
            this.cbApplyVPT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbApplyVPT.AutoSize = true;
            this.cbApplyVPT.Location = new System.Drawing.Point(8, 357);
            this.cbApplyVPT.Margin = new System.Windows.Forms.Padding(8, 8, 3, 3);
            this.cbApplyVPT.Name = "cbApplyVPT";
            this.cbApplyVPT.Size = new System.Drawing.Size(149, 17);
            this.cbApplyVPT.TabIndex = 3;
            this.cbApplyVPT.Text = "Apply view point transform";
            this.cbApplyVPT.UseVisualStyleBackColor = true;
            this.cbApplyVPT.CheckedChanged += new System.EventHandler(this.cbApplyVPT_CheckedChanged);
            // 
            // btnHorizontalView
            // 
            this.btnHorizontalView.Location = new System.Drawing.Point(80, 11);
            this.btnHorizontalView.Name = "btnHorizontalView";
            this.btnHorizontalView.Size = new System.Drawing.Size(66, 23);
            this.btnHorizontalView.TabIndex = 2;
            this.btnHorizontalView.Text = "Horizontal";
            this.btnHorizontalView.UseVisualStyleBackColor = true;
            this.btnHorizontalView.Click += new System.EventHandler(this.btnHorizontalView_Click);
            // 
            // btnProfileView
            // 
            this.btnProfileView.Location = new System.Drawing.Point(152, 11);
            this.btnProfileView.Name = "btnProfileView";
            this.btnProfileView.Size = new System.Drawing.Size(60, 23);
            this.btnProfileView.TabIndex = 1;
            this.btnProfileView.Text = "Profile";
            this.btnProfileView.UseVisualStyleBackColor = true;
            this.btnProfileView.Click += new System.EventHandler(this.btnProfileView_Click);
            // 
            // btnFrontalView
            // 
            this.btnFrontalView.Location = new System.Drawing.Point(6, 11);
            this.btnFrontalView.Name = "btnFrontalView";
            this.btnFrontalView.Size = new System.Drawing.Size(68, 23);
            this.btnFrontalView.TabIndex = 0;
            this.btnFrontalView.Text = "Frontal";
            this.btnFrontalView.UseVisualStyleBackColor = true;
            this.btnFrontalView.Click += new System.EventHandler(this.btnFrontalView_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.cbUseFlatLightning);
            this.tabPage3.Controls.Add(this.label39);
            this.tabPage3.Controls.Add(this.btnLightColor);
            this.tabPage3.Controls.Add(this.tbModelSpecularShines);
            this.tabPage3.Controls.Add(this.label37);
            this.tabPage3.Controls.Add(this.label33);
            this.tabPage3.Controls.Add(this.tbModelAmbient);
            this.tabPage3.Controls.Add(this.label34);
            this.tabPage3.Controls.Add(this.tbModelSpecular);
            this.tabPage3.Controls.Add(this.label35);
            this.tabPage3.Controls.Add(this.tbModelDiffuse);
            this.tabPage3.Controls.Add(this.label36);
            this.tabPage3.Controls.Add(this.label32);
            this.tabPage3.Controls.Add(this.tbLightAmbient);
            this.tabPage3.Controls.Add(this.label29);
            this.tabPage3.Controls.Add(this.tbLightSpecular);
            this.tabPage3.Controls.Add(this.label30);
            this.tabPage3.Controls.Add(this.tbLightDiffuse);
            this.tabPage3.Controls.Add(this.label31);
            this.tabPage3.Controls.Add(this.btnApplyLight);
            this.tabPage3.Controls.Add(this.btnResetLight);
            this.tabPage3.Controls.Add(this.cbLight);
            this.tabPage3.Controls.Add(this.tbLightX);
            this.tabPage3.Controls.Add(this.label26);
            this.tabPage3.Controls.Add(this.tbLightZ);
            this.tabPage3.Controls.Add(this.label27);
            this.tabPage3.Controls.Add(this.tbLightY);
            this.tabPage3.Controls.Add(this.label28);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(218, 512);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Lightning";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // cbUseFlatLightning
            // 
            this.cbUseFlatLightning.AutoSize = true;
            this.cbUseFlatLightning.Location = new System.Drawing.Point(106, 11);
            this.cbUseFlatLightning.Name = "cbUseFlatLightning";
            this.cbUseFlatLightning.Size = new System.Drawing.Size(104, 17);
            this.cbUseFlatLightning.TabIndex = 46;
            this.cbUseFlatLightning.Text = "Use flat lightning";
            this.cbUseFlatLightning.UseVisualStyleBackColor = true;
            this.cbUseFlatLightning.CheckedChanged += new System.EventHandler(this.cbUseFlatLightning_CheckedChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(70, 227);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(31, 13);
            this.label39.TabIndex = 45;
            this.label39.Text = "Color";
            // 
            // btnLightColor
            // 
            this.btnLightColor.BackColor = System.Drawing.Color.Transparent;
            this.btnLightColor.Location = new System.Drawing.Point(110, 222);
            this.btnLightColor.Name = "btnLightColor";
            this.btnLightColor.Size = new System.Drawing.Size(100, 23);
            this.btnLightColor.TabIndex = 44;
            this.btnLightColor.UseVisualStyleBackColor = false;
            this.btnLightColor.Click += new System.EventHandler(this.btnLightColor_Click);
            // 
            // tbModelSpecularShines
            // 
            this.tbModelSpecularShines.Location = new System.Drawing.Point(110, 364);
            this.tbModelSpecularShines.Name = "tbModelSpecularShines";
            this.tbModelSpecularShines.Size = new System.Drawing.Size(100, 20);
            this.tbModelSpecularShines.TabIndex = 43;
            this.tbModelSpecularShines.Text = "3";
            this.tbModelSpecularShines.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(22, 367);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(82, 13);
            this.label37.TabIndex = 42;
            this.label37.Text = "Specular shines";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(8, 264);
            this.label33.Margin = new System.Windows.Forms.Padding(8, 24, 3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(75, 13);
            this.label33.TabIndex = 41;
            this.label33.Text = "Model material";
            // 
            // tbModelAmbient
            // 
            this.tbModelAmbient.Location = new System.Drawing.Point(110, 286);
            this.tbModelAmbient.Margin = new System.Windows.Forms.Padding(3, 16, 3, 3);
            this.tbModelAmbient.Name = "tbModelAmbient";
            this.tbModelAmbient.Size = new System.Drawing.Size(100, 20);
            this.tbModelAmbient.TabIndex = 40;
            this.tbModelAmbient.Text = "1";
            this.tbModelAmbient.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(29, 289);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(75, 13);
            this.label34.TabIndex = 39;
            this.label34.Text = "Ambient factor";
            // 
            // tbModelSpecular
            // 
            this.tbModelSpecular.Location = new System.Drawing.Point(110, 338);
            this.tbModelSpecular.Name = "tbModelSpecular";
            this.tbModelSpecular.Size = new System.Drawing.Size(100, 20);
            this.tbModelSpecular.TabIndex = 38;
            this.tbModelSpecular.Text = "1";
            this.tbModelSpecular.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(25, 341);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(79, 13);
            this.label35.TabIndex = 37;
            this.label35.Text = "Specular factor";
            // 
            // tbModelDiffuse
            // 
            this.tbModelDiffuse.Location = new System.Drawing.Point(110, 312);
            this.tbModelDiffuse.Name = "tbModelDiffuse";
            this.tbModelDiffuse.Size = new System.Drawing.Size(100, 20);
            this.tbModelDiffuse.TabIndex = 36;
            this.tbModelDiffuse.Text = "1";
            this.tbModelDiffuse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(34, 315);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(70, 13);
            this.label36.TabIndex = 35;
            this.label36.Text = "Diffuse factor";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(8, 122);
            this.label32.Margin = new System.Windows.Forms.Padding(8, 24, 3, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(30, 13);
            this.label32.TabIndex = 34;
            this.label32.Text = "Light";
            // 
            // tbLightAmbient
            // 
            this.tbLightAmbient.Location = new System.Drawing.Point(110, 144);
            this.tbLightAmbient.Margin = new System.Windows.Forms.Padding(3, 16, 3, 3);
            this.tbLightAmbient.Name = "tbLightAmbient";
            this.tbLightAmbient.Size = new System.Drawing.Size(100, 20);
            this.tbLightAmbient.TabIndex = 33;
            this.tbLightAmbient.Text = "0,25";
            this.tbLightAmbient.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(18, 147);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(86, 13);
            this.label29.TabIndex = 32;
            this.label29.Text = "Ambient intensity";
            // 
            // tbLightSpecular
            // 
            this.tbLightSpecular.Location = new System.Drawing.Point(110, 196);
            this.tbLightSpecular.Name = "tbLightSpecular";
            this.tbLightSpecular.Size = new System.Drawing.Size(100, 20);
            this.tbLightSpecular.TabIndex = 31;
            this.tbLightSpecular.Text = "1,2";
            this.tbLightSpecular.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(14, 199);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(90, 13);
            this.label30.TabIndex = 30;
            this.label30.Text = "Specular intensity";
            // 
            // tbLightDiffuse
            // 
            this.tbLightDiffuse.Location = new System.Drawing.Point(110, 170);
            this.tbLightDiffuse.Name = "tbLightDiffuse";
            this.tbLightDiffuse.Size = new System.Drawing.Size(100, 20);
            this.tbLightDiffuse.TabIndex = 29;
            this.tbLightDiffuse.Text = "0,7";
            this.tbLightDiffuse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(23, 173);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(81, 13);
            this.label31.TabIndex = 28;
            this.label31.Text = "Diffuse intensity";
            // 
            // btnApplyLight
            // 
            this.btnApplyLight.Location = new System.Drawing.Point(8, 34);
            this.btnApplyLight.Margin = new System.Windows.Forms.Padding(8, 3, 3, 3);
            this.btnApplyLight.Name = "btnApplyLight";
            this.btnApplyLight.Size = new System.Drawing.Size(93, 24);
            this.btnApplyLight.TabIndex = 27;
            this.btnApplyLight.Text = "Apply";
            this.btnApplyLight.UseVisualStyleBackColor = true;
            this.btnApplyLight.Click += new System.EventHandler(this.btnApplyLight_Click);
            // 
            // btnResetLight
            // 
            this.btnResetLight.Location = new System.Drawing.Point(117, 34);
            this.btnResetLight.Margin = new System.Windows.Forms.Padding(3, 3, 8, 3);
            this.btnResetLight.Name = "btnResetLight";
            this.btnResetLight.Size = new System.Drawing.Size(93, 24);
            this.btnResetLight.TabIndex = 26;
            this.btnResetLight.Text = "Reset";
            this.btnResetLight.UseVisualStyleBackColor = true;
            this.btnResetLight.Click += new System.EventHandler(this.btnResetLight_Click);
            // 
            // cbLight
            // 
            this.cbLight.AutoSize = true;
            this.cbLight.Location = new System.Drawing.Point(8, 11);
            this.cbLight.Name = "cbLight";
            this.cbLight.Size = new System.Drawing.Size(81, 17);
            this.cbLight.TabIndex = 17;
            this.cbLight.Text = "Enable light";
            this.cbLight.UseVisualStyleBackColor = true;
            this.cbLight.CheckedChanged += new System.EventHandler(this.cbLight_CheckedChanged);
            // 
            // tbLightX
            // 
            this.tbLightX.Location = new System.Drawing.Point(21, 82);
            this.tbLightX.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tbLightX.Name = "tbLightX";
            this.tbLightX.Size = new System.Drawing.Size(47, 20);
            this.tbLightX.TabIndex = 16;
            this.tbLightX.Text = "1000";
            this.tbLightX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(2, 85);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(14, 13);
            this.label26.TabIndex = 15;
            this.label26.Text = "X";
            // 
            // tbLightZ
            // 
            this.tbLightZ.Location = new System.Drawing.Point(163, 82);
            this.tbLightZ.Margin = new System.Windows.Forms.Padding(2, 3, 4, 3);
            this.tbLightZ.Name = "tbLightZ";
            this.tbLightZ.Size = new System.Drawing.Size(47, 20);
            this.tbLightZ.TabIndex = 14;
            this.tbLightZ.Text = "1000";
            this.tbLightZ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(144, 85);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(14, 13);
            this.label27.TabIndex = 13;
            this.label27.Text = "Z";
            // 
            // tbLightY
            // 
            this.tbLightY.Location = new System.Drawing.Point(92, 82);
            this.tbLightY.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.tbLightY.Name = "tbLightY";
            this.tbLightY.Size = new System.Drawing.Size(47, 20);
            this.tbLightY.TabIndex = 12;
            this.tbLightY.Text = "1000";
            this.tbLightY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(73, 85);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(14, 13);
            this.label28.TabIndex = 11;
            this.label28.Text = "Y";
            // 
            // colorDialog
            // 
            this.colorDialog.Color = System.Drawing.Color.White;
            // 
            // openModelDialog
            // 
            this.openModelDialog.Filter = "Model files|*.obj";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 562);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(840, 600);
            this.Name = "MainForm";
            this.Text = "Курсовой проект по дисциплине 3D моделирование. Автор: Петровский А. В. гр. 10752" +
    "9";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRender)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbTessFactor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbHolePosition)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbRender;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnRedrawWire;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lblHolePosition;
        private System.Windows.Forms.Label lblTessValue;
        private System.Windows.Forms.TrackBar trbTessFactor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbModelSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TrackBar trbHolePosition;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbInnerRadius;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbOuterRadius;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnModel;
        private System.Windows.Forms.Button btnBox;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbRZ;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbRY;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbRX;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbSZ;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbSY;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbSX;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbDZ;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbDY;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbDX;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnRZSub;
        private System.Windows.Forms.Button btnRZAdd;
        private System.Windows.Forms.Button btnRYSub;
        private System.Windows.Forms.Button btnRYAdd;
        private System.Windows.Forms.Button btnRXSub;
        private System.Windows.Forms.Button btnRXAdd;
        private System.Windows.Forms.Button btnHorizontalView;
        private System.Windows.Forms.Button btnProfileView;
        private System.Windows.Forms.Button btnFrontalView;
        private System.Windows.Forms.Button btnRedrawFace;
        private System.Windows.Forms.CheckBox cbApplyVPT;
        private System.Windows.Forms.Button btnVPPhSub;
        private System.Windows.Forms.Button btnVPPhAdd;
        private System.Windows.Forms.Button btnVPThSub;
        private System.Windows.Forms.Button btnVPThAdd;
        private System.Windows.Forms.TextBox tbVPTh;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbVPPh;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbVPRo;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbObliqueAngle;
        private System.Windows.Forms.TextBox tbObliqueL;
        private System.Windows.Forms.Button btnOblique;
        private System.Windows.Forms.TextBox tbPerspectiveD;
        private System.Windows.Forms.Button btnPerspective;
        private System.Windows.Forms.TextBox tbAxonometrixKsi;
        private System.Windows.Forms.TextBox tbAxonometrixPhi;
        private System.Windows.Forms.Button btnAxonometric;
        private System.Windows.Forms.Button btnApplyTransform;
        private System.Windows.Forms.Button btnSZSub;
        private System.Windows.Forms.Button f;
        private System.Windows.Forms.Button btnSYSub;
        private System.Windows.Forms.Button btnSYAdd;
        private System.Windows.Forms.Button btnSXSub;
        private System.Windows.Forms.Button btnSXAdd;
        private System.Windows.Forms.Button btnDZSub;
        private System.Windows.Forms.Button btnDZAdd;
        private System.Windows.Forms.Button btnDYSub;
        private System.Windows.Forms.Button btnDYAdd;
        private System.Windows.Forms.Button btnDXSub;
        private System.Windows.Forms.Button btnDXAdd;
        private System.Windows.Forms.Button btnVPRoSub;
        private System.Windows.Forms.Button btnVPRoAdd;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cbDrawZ;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbFovY;
        private System.Windows.Forms.Button btnFrustum;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbZFar;
        private System.Windows.Forms.TextBox tbZNear;
        private System.Windows.Forms.Label lblBufferTime;
        private System.Windows.Forms.Label lblRenderTime;
        private System.Windows.Forms.Button btnApplyLight;
        private System.Windows.Forms.Button btnResetLight;
        private System.Windows.Forms.CheckBox cbLight;
        private System.Windows.Forms.TextBox tbLightX;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbLightZ;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbLightY;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tbModelAmbient;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox tbModelSpecular;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tbModelDiffuse;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tbLightAmbient;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbLightSpecular;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tbLightDiffuse;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tbModelSpecularShines;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btnLightColor;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Button btnBackColor;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.OpenFileDialog openModelDialog;
        private System.Windows.Forms.CheckBox cbUseFlatLightning;
        private System.Windows.Forms.CheckBox cbUseZBuffer;
        private System.Windows.Forms.Button btnWireColor;
    }
}

