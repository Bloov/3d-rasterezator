﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace Termpaper_3D_Graphics
{
    class Edge
    {
        private int start, end;
        private Color color;

        public Edge(int start, int end, Color color)
        {
            this.start = start;
            this.end = end;
            this.color = color;
        }

        public Color EdgeColor
        {
            get { return color; }
        }

        public int Start
        {
            get { return start; }            
        }

        public int End
        {
            get { return end; }
        }
    }

    class Face
    {
        private Model model;
        internal int[] vertexes;
        private bool[] fixedNormalFlags; 
        private Vector3[] vertexNormals;
        internal Vector3 normal;
        
        public Face(Model model, Color color)
        {
            this.model = model;
            FaceColor = color;
            AmbientFactor = DiffuseFactor = SpecularFactor = 1;

            vertexes = new int[3] { -1, -1, -1 };
            fixedNormalFlags = new bool[3] { true, true, true };
            normal = new Vector3(0, 0, 1);            
            vertexNormals = new Vector3[3] { normal, normal, normal };
        }

        public Color FaceColor { get; set; }

        public double AmbientFactor { get; set; }

        public double DiffuseFactor { get; set; }

        public double SpecularFactor { get; set; }

        public double SpecularShines { get; set; }

        public Vector3 Normal
        {
            get { return normal; }
        }

        private bool ValidVertexIndex(int index)
        {
            return (index >= 0) && (index < 3);
        }

        public void SetVertex(int index, int vertex, bool fixedNormal = true)
        {
            if (ValidVertexIndex(index) && model.ValidVertexIndex(vertex))
            {
                vertexes[index] = vertex;
                fixedNormalFlags[index] = fixedNormal;
            }
        }               

        public int GetVertexInModel(int index)
        {
            if (ValidVertexIndex(index))
                return vertexes[index];
            else
                throw new Exception("Incorrect vertex index in face");
        }

        public int GetVertexInFace(int vertex)
        {
            if (vertexes[0] == vertex)
                return 0;
            else if (vertexes[1] == vertex)
                return 1;
            else if (vertexes[2] == vertex)
                return 2;
            else
                return -1;
        }

        public bool ContainsVertex(int vertex)
        {
            return (vertexes[0] == vertex) || (vertexes[1] == vertex) || (vertexes[2] == vertex);
        }

        public Vector3 GetVertex(int index)
        {
            if (ValidVertexIndex(index))
                return model.GetVertexUnsafe(vertexes[index]);            
            else
                throw new Exception("Incorrect vertex index in face");
        }

        public Vector3 GetVertexUnsafe(int index)
        {
            return model.GetVertexUnsafe(vertexes[index]);
        }

        public void SetVertexNormal(int index, Vector3 normal)
        {
            if (ValidVertexIndex(index))
                vertexNormals[index] = normal;
        }

        public Vector3 GetVertexNormal(int index)
        {
            if (ValidVertexIndex(index))
                return vertexNormals[index];
            else
                throw new Exception("Incorrect vertex index in face");
        }

        public Vector3 GetVertexNormalUnsafe(int index)
        {
            return vertexNormals[index];            
        }

        public bool GetVertexNormalFixed(int index)
        {
            if (ValidVertexIndex(index))
                return fixedNormalFlags[index];
            else
                throw new Exception("Incorrect vertex index in face");

        }

        public Vector3 CalculateNormal()
        {
            var v0 = GetVertexUnsafe(0);
            var e1 = GetVertexUnsafe(1) - v0;
            var e2 = GetVertexUnsafe(2) - v0;
            normal = e1.CrossProduct(e2);
            normal.Normalize();
            return normal;
        }
    }

    class Model
    {
        private List<Vector3> vertexes;
        private List<Edge> visibleEdges;
        private List<Face> faces;

        public Model()
        {
            vertexes = new List<Vector3>();
            visibleEdges = new List<Edge>();
            faces = new List<Face>();
        }

        public int VertexCount
        {
            get { return vertexes.Count; }
        }

        public bool ValidVertexIndex(int index)
        {
            return (index >= 0) && (index < vertexes.Count);
        }

        public int AddVertex(Vector3 point)
        {
            vertexes.Add(point);
            return vertexes.Count - 1;
        }        

        public Vector3 GetVertex(int index)
        {
            if (ValidVertexIndex(index))
                return vertexes[index];
            else
                throw new Exception("Incorrect vertex index in model");
        }

        public Vector3 GetVertexUnsafe(int index)
        {
            return vertexes[index];            
        }

        public int EdgesCount
        {
            get { return visibleEdges.Count; }
        } 

        public bool ValidEdgeIndex(int index)
        {
            return (index >= 0) && (index < visibleEdges.Count);
        }

        public int AddEdge(int vertexA, int vertexB, Color color)
        {
            if (ValidVertexIndex(vertexA) && ValidVertexIndex(vertexB) && (vertexA != vertexB))
            {
                var edge = new Edge(vertexA, vertexB, color);
                visibleEdges.Add(edge);
                return visibleEdges.Count - 1;
            }
            else
                throw new Exception("Incorrect vertexes index for edge");
        }

        public Edge GetEdge(int index)
        {
            if (ValidEdgeIndex(index))
                return visibleEdges[index];
            else
                throw new Exception("Incorrect edge index in model");
        }

        public int FacesCount
        {
            get { return faces.Count; }
        }

        public bool ValidFaceIndex(int index)
        {
            return (index >= 0) && (index < faces.Count);
        }

        public Face GetFace(int index)
        {
            if (ValidFaceIndex(index))
                return faces[index];
            else
                throw new Exception("Incorrect face index in model");
        }

        public int AddFace(int[] vert, bool [] vertNormalFixed, Color color)
        {
            if ((vert == null) || (vertNormalFixed == null) || (vert.Length != vertNormalFixed.Length) ||
                (vert.Length != 3) || !vert.All(i => ValidVertexIndex(i)))
            {
                throw new Exception("Incorrect face data");
            }

            var face = new Face(this, color);
            for (int i = 0; i < vert.Length; ++i)
                face.SetVertex(i, vert[i], vertNormalFixed[i]);
            faces.Add(face);
            return faces.Count - 1;            
        }                

        public void SetMaterial(double kAmbient, double kDiffuse, double kSpecular, double kSpecularShines)
        {
            foreach (var face in faces)
            {
                face.AmbientFactor = kAmbient;
                face.DiffuseFactor = kDiffuse;
                face.SpecularFactor = kSpecular;
                face.SpecularShines = kSpecularShines;
            }
        }        

        private Dictionary<int, List<Face>> MapVertex2Edges()
        {
            var result = new Dictionary<int, List<Face>>();
            for (int vertex = 0; vertex < vertexes.Count; ++vertex)
            {
                var list = new List<Face>();
                result.Add(vertex, list);
            }

            foreach (var face in faces)
            {
                result[face.vertexes[0]].Add(face);
                result[face.vertexes[1]].Add(face);
                result[face.vertexes[2]].Add(face);
            }

            return result;
        }

        public void CalculateNormals()
        {
            var vertex2Faces = MapVertex2Edges();

            foreach (var face in faces)
                face.CalculateNormal();

            foreach (var face in faces)            
                for (int vertex = 0; vertex < 3; ++vertex)
                {
                    var vertexInModel = face.GetVertexInModel(vertex);                    
                    var normal = face.Normal;
                    
                    if (!face.GetVertexNormalFixed(vertex))
                        foreach (var neighbor in vertex2Faces[vertexInModel])
                        {
                            var vertexInNeighbor = neighbor.GetVertexInFace(vertexInModel);
                            if ((neighbor != face) && !neighbor.GetVertexNormalFixed(vertexInNeighbor))
                                normal += neighbor.Normal;                        
                        }

                    if (normal.Length != 0)
                        normal.Normalize();

                    face.SetVertexNormal(vertex, normal);
                }            
        }

        public void NormilizeToSize(double size)
        {
            double minX, maxX, minY, maxY, minZ, maxZ;

            minX = maxX = vertexes[0].x;
            minY = maxY = vertexes[0].y;
            minZ = maxZ = vertexes[0].z;

            foreach (var vertex in vertexes)
            {
                if (vertex.X < minX)
                    minX = vertex.x;
                if (vertex.X > maxX)
                    maxX = vertex.x;

                if (vertex.Y < minY)
                    minY = vertex.y;
                if (vertex.Y > maxY)
                    maxY = vertex.y;

                if (vertex.Z < minZ)
                    minZ = vertex.z;
                if (vertex.Z > maxZ)
                    maxZ = vertex.z;
            }

            var widthX = maxX - minX;
            var widthY = maxY - minY;
            var widthZ = maxZ - minZ;
            var width = 0.0;
            if (widthX > widthY)
                width = widthX;
            else
                width = widthY;
            if (widthZ > width)
                width = widthZ;

            var factor = size / width;
            for (var i = 0; i < vertexes.Count; ++i)
                vertexes[i] = vertexes[i] * factor;
        }

        public void RecalculateNormals(Matrix4x4 modelTransform)
        {
            var vertex2Faces = MapVertex2Edges();
            var transformedVertex = new List<Vector3>();            

            for (int i = 0; i < VertexCount; ++i)            
                transformedVertex.Add(new Vector3(new Vector4(vertexes[i]).Multiply(modelTransform)));            

            foreach (var face in faces)
            {
                var p0 = new Vector3(transformedVertex[face.GetVertexInModel(0)]);
                var p1 = new Vector3(transformedVertex[face.GetVertexInModel(1)]);
                var p2 = new Vector3(transformedVertex[face.GetVertexInModel(2)]);
                var normal = (p1 - p0).CrossProduct(p2 - p0);
                normal.Normalize();
                face.normal = normal;                
            }                

            foreach (var face in faces)
                for (int vertex = 0; vertex < 3; ++vertex)
                {
                    var vertexInModel = face.GetVertexInModel(vertex);
                    var normal = face.Normal;

                    if (!face.GetVertexNormalFixed(vertex))
                        foreach (var neighbor in vertex2Faces[vertexInModel])
                        {
                            var vertexInNeighbor = neighbor.GetVertexInFace(vertexInModel);
                            if ((neighbor != face) && !neighbor.GetVertexNormalFixed(vertexInNeighbor))
                                normal += neighbor.Normal;
                        }

                    if (normal.Length != 0)
                        normal.Normalize();

                    face.SetVertexNormal(vertex, normal);
                }            
        }
    }

    static class ModelMaker
    {
        public static Model MakeBox(double size)
        {
            size /= 2.0;
            var result = new Model();
            
            result.AddVertex(new Vector3(-size, -size, -size));
            result.AddVertex(new Vector3(size, -size, -size));
            result.AddVertex(new Vector3(size, size, -size));
            result.AddVertex(new Vector3(-size, size, -size));

            result.AddVertex(new Vector3(-size, size, size));
            result.AddVertex(new Vector3(size, size, size));
            result.AddVertex(new Vector3(size, -size, size));
            result.AddVertex(new Vector3(-size, -size, size));

            result.AddEdge(0, 1, Color.Yellow);
            result.AddEdge(1, 2, Color.Yellow);
            result.AddEdge(2, 3, Color.Yellow);
            result.AddEdge(3, 0, Color.Yellow);

            result.AddEdge(4, 5, Color.Yellow);
            result.AddEdge(5, 6, Color.Yellow);
            result.AddEdge(6, 7, Color.Yellow);
            result.AddEdge(7, 4, Color.Yellow);

            result.AddEdge(0, 7, Color.Yellow);
            result.AddEdge(1, 6, Color.Yellow);
            result.AddEdge(2, 5, Color.Yellow);
            result.AddEdge(3, 4, Color.Yellow);

            result.AddFace(new int[] { 0, 2, 1 }, new bool[] { true, true, true }, Color.FromArgb(255, 220, 120, 120));
            result.AddFace(new int[] { 0, 3, 2 }, new bool[] { true, true, true }, Color.FromArgb(255, 220, 120, 120));

            result.AddFace(new int[] { 0, 6, 7 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 220, 120));
            result.AddFace(new int[] { 0, 1, 6 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 220, 120));

            result.AddFace(new int[] { 7, 6, 5 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 120, 220));
            result.AddFace(new int[] { 7, 5, 4 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 120, 220));

            result.AddFace(new int[] { 3, 4, 5 }, new bool[] { true, true, true }, Color.FromArgb(255, 220, 120, 220));
            result.AddFace(new int[] { 2, 3, 5 }, new bool[] { true, true, true }, Color.FromArgb(255, 220, 120, 220));

            result.AddFace(new int[] { 1, 2, 5 }, new bool[] { true, true, true }, Color.FromArgb(255, 220, 220, 120));
            result.AddFace(new int[] { 1, 5, 6 }, new bool[] { true, true, true }, Color.FromArgb(255, 220, 220, 120));

            result.AddFace(new int[] { 7, 4, 0 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 220, 220));
            result.AddFace(new int[] { 4, 3, 0 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 220, 220));

            result.CalculateNormals();

            return result;
        }

        private static int GetAnglesCount(int tessFactor)
        {
            switch (tessFactor)
            {
                case 1: return 8;
                case 2: return 10;
                case 3: return 14;
                case 4: return 18;
                case 5: return 22;
                case 6: return 28;
                case 7: return 32;
                case 8: return 48;
                case 9: return 64;
                case 10: return 96;
                default: return 8;
            }
        }

        public static Model MakeCylinder(int tessFactor, double r1, double r2, double r, double h)
        {
            var result = new Model();
            
            if ((r1 <= r2) || (r >= r1 - r2))
            {
                r1 = 60;
                r2 = 10;
                r = 0;
            }

            var anglesCount = GetAnglesCount(tessFactor);
            for (int i = 0; i < anglesCount; ++i)
            {
                var angle = i * 360 / (double)anglesCount;
                var cosa = Math.Cos(angle * Math.PI / 180.0);
                var sina = Math.Sin(angle * Math.PI / 180.0);

                result.AddVertex(new Vector3(r1 * cosa, r1 * sina, h * 0.5));
                result.AddVertex(new Vector3(r1 * cosa, r1 * sina, -h * 0.5));
                result.AddVertex(new Vector3(r + r2 * cosa, r2 * sina, h * 0.5));
                result.AddVertex(new Vector3(r + r2 * cosa, r2 * sina, -h * 0.5));
            }

            for (int i = 0; i < anglesCount; ++i)
            {
                var io = i * 4;
                var it = (i + 1) * 4;

                result.AddEdge(io,     io + 1, Color.FromArgb(255, 220, 60, 60));
                result.AddEdge(io + 2, io + 3, Color.FromArgb(255, 220, 60, 60));
                result.AddEdge(io,     io + 2, Color.FromArgb(255, 220, 60, 60));
                result.AddEdge(io + 1, io + 3, Color.FromArgb(255, 220, 60, 60));

                if (i < anglesCount - 1)
                {
                    result.AddEdge(io,     it,     Color.FromArgb(255, 220, 60, 60));
                    result.AddEdge(io + 1, it + 1, Color.FromArgb(255, 220, 60, 60));
                    result.AddEdge(io + 2, it + 2, Color.FromArgb(255, 220, 60, 60));
                    result.AddEdge(io + 3, it + 3, Color.FromArgb(255, 220, 60, 60));

                    result.AddFace(new int[] { io, io + 1, it + 1 }, new bool[] { false, false, false }, Color.FromArgb(255, 120, 120, 120));
                    result.AddFace(new int[] { io, it + 1, it + 0 }, new bool[] { false, false, false }, Color.FromArgb(255, 120, 120, 120));

                    result.AddFace(new int[] { io, it + 0, io + 2 }, new bool[] { false, false, false }, Color.FromArgb(255, 160, 135, 220));
                    result.AddFace(new int[] { it, it + 2, io + 2 }, new bool[] { false, false, false }, Color.FromArgb(255, 160, 135, 220));

                    result.AddFace(new int[] { io + 3, io + 2, it + 3 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 120, 120));
                    result.AddFace(new int[] { it + 3, io + 2, it + 2 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 120, 120));

                    result.AddFace(new int[] { io + 1, io + 3, it + 1 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 120, 220));
                    result.AddFace(new int[] { it + 1, io + 3, it + 3 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 120, 220));                    
                }
                else
                {
                    result.AddEdge(io,     0, Color.FromArgb(255, 220, 60, 60));
                    result.AddEdge(io + 1, 1, Color.FromArgb(255, 220, 60, 60));
                    result.AddEdge(io + 2, 2, Color.FromArgb(255, 220, 60, 60));
                    result.AddEdge(io + 3, 3, Color.FromArgb(255, 220, 60, 60));

                    result.AddFace(new int[] { io, io + 1, 1 }, new bool[] { false, false, false }, Color.FromArgb(255, 120, 120, 120));
                    result.AddFace(new int[] { io, 1, 0 }, new bool[] { false, false, false }, Color.FromArgb(255, 120, 120, 120));

                    result.AddFace(new int[] { io, 0, io + 2 }, new bool[] { false, false, false }, Color.FromArgb(255, 160, 135, 220));
                    result.AddFace(new int[] { 0, 2, io + 2 }, new bool[] { false, false, false }, Color.FromArgb(255, 160, 135, 220));

                    result.AddFace(new int[] { io + 3, io + 2, 3 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 120, 120));
                    result.AddFace(new int[] { 3, io + 2, 2 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 120, 120));

                    result.AddFace(new int[] { io + 1, io + 3, 1 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 120, 220));
                    result.AddFace(new int[] { 1, io + 3, 3 }, new bool[] { true, true, true }, Color.FromArgb(255, 120, 120, 220));
                }                
            }

            result.CalculateNormals();

            return result;
        }

        private static List<string> ParseStringToBlocks(string str)
        {
            var list = new List<string>();
            var builder = new StringBuilder();
            for (var i = 0; i < str.Length; ++i)
            {
                if (str[i] != ' ')
                    builder.Append(str[i]);
                else
                {
                    if (builder.Length > 0)
                        list.Add(builder.ToString());
                    builder.Clear();
                }
            }
            if (builder.Length > 0)
                list.Add(builder.ToString());

            return list;
        }

        public static Model LoadModel(string modelFile, double size)
        {
            var result = new Model();
            var file = new StreamReader(modelFile);
            try
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (string.IsNullOrWhiteSpace(line) || (line[0] == '#'))
                        continue;

                    var blocks = ParseStringToBlocks(line);
                    if (blocks.Count <= 1)
                        continue;

                    switch (blocks[0])
                    {
                        case "v":
                            var v1 = double.Parse(blocks[1], CultureInfo.InvariantCulture);
                            var v2 = double.Parse(blocks[2], CultureInfo.InvariantCulture);
                            var v3 = double.Parse(blocks[3], CultureInfo.InvariantCulture);

                            result.AddVertex(new Vector3(v1, v2, v3));

                            break;

                        case "f":
                            int f1, f2, f3;
                            int.TryParse(blocks[1], out f1);
                            int.TryParse(blocks[2], out f2);
                            int.TryParse(blocks[3], out f3);
                            --f1;
                            --f2;
                            --f3;

                            result.AddEdge(f1, f2, Color.Yellow);
                            result.AddEdge(f2, f3, Color.Yellow);
                            result.AddEdge(f3, f1, Color.Yellow);

                            result.AddFace(new int[] { f1, f2, f3 }, new bool[] { false, false, false }, Color.Gray);

                            break;
                    }
                }
            }
            finally
            {
                file.Close();
            }

            result.CalculateNormals();
            result.NormilizeToSize(size);

            return result;
        }
    }
}
