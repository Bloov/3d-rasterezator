﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Termpaper_3D_Graphics
{
    static class Utils
    {
        public static int Pow(int value, int exp)
        {
            var result = 1;
            while (exp > 0)
            {
                --exp;
                result *= value;
            }
            return result;
        }

        public static int ClampDouble(double value)
        {
            if (value > int.MaxValue)
                return int.MaxValue;
            else if (value < int.MinValue)
                return int.MinValue;
            else if (!double.IsNaN(value))
                return Convert.ToInt32(value);
            else
                return 0;

        }

        public static int ClampInt(int value, int min, int max)
        {
            if (value < min)
                return min;
            else if (value > max)
                return max;
            else
                return value;
        }

        public static void Swap<T>(ref T a, ref T b)
        {
            T temp = a;
            a = b;
            b = temp;
        }                

        public static Color BlendLight(Color color, double intensy, Color lightColor)
        {
            var r = color.R * (int)(intensy * lightColor.R) / 255;
            var g = color.G * (int)(intensy * lightColor.G) / 255;
            var b = color.B * (int)(intensy * lightColor.B) / 255;
            r = ClampInt(r, 0, 255);
            g = ClampInt(g, 0, 255);
            b = ClampInt(b, 0, 255);
            return Color.FromArgb(255, r, g, b);
        }
    }    
}
