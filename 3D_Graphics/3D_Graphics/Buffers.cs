﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Termpaper_3D_Graphics
{
    class ImageBuffer
    {
        private int width, height;        
        private int[,] pixels;
        private object lockObject;

        public ImageBuffer(int width, int height, Color backColor)
        {
            lockObject = new object();

            this.width = width;
            this.height = height;
            
            pixels = new int[width, height];
            var intBackColor = backColor.ToArgb();
            for (int x = 0; x < width; ++x)
                for (int y = 0; y < height; ++y)
                    pixels[x, y] = intBackColor;
        } 
       
        public static ImageBuffer FromBitmap(Bitmap bitmap, Color backColor)
        {
            var buffer = new ImageBuffer(bitmap.Width, bitmap.Height, backColor);

            BitmapData bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadOnly, bitmap.PixelFormat);
            var stride = bitmapData.Stride;
            var data = new byte[stride * bitmapData.Height];
            Marshal.Copy(bitmapData.Scan0, data, 0, data.Length);
            bitmap.UnlockBits(bitmapData);                        

            int delta = stride / bitmap.Width;
            for (int y = 0; y < bitmap.Height; ++y)
            {
                int offset = y * stride;
                for (int x = 0; x < bitmap.Width; ++x)
                {
                    int index = offset + x * delta;
                    var color = Color.FromArgb(data[index], data[index + 1], data[index + 2], data[index + 3]);
                    buffer[x, y] = color.ToArgb();
                }
            }

            return buffer;
        }

        public int Width
        {
            get
            {
                return width;
            }
        }

        public int Height
        {
            get
            {
                return height;
            }
        }        

        private bool ValidIndex(int x, int y)
        {
            return (x >= 0) && (x < width) && (y >= 0) && (y < height);
        }

        public int this[int x, int y]
        {
            get
            {
                return ValidIndex(x, y) ? pixels[x, y] : 0;                
            }
            set
            {
                if (ValidIndex(x, y))
                    pixels[x, y] = value;
            }
        }

        public int GetUnsafe(int x, int y)
        {
            return pixels[x, y];
        }

        public void SetUnsafe(int x, int y, int value)
        {
            pixels[x, y] = value;
        }

        public Color GetColor(int x, int y)
        {
            return ValidIndex(x, y) ? Color.FromArgb(pixels[x, y]) : Color.Black;
        }

        public void SetColor(int x, int y, Color color)     
        {
            if (ValidIndex(x, y))
                pixels[x, y] = color.ToArgb();
        }        

        public Bitmap GetAsBitmap()
        {
            var bitmap = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            unsafe
            {
                BitmapData bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                    ImageLockMode.WriteOnly, bitmap.PixelFormat);

                int bytesPerPixel = Bitmap.GetPixelFormatSize(bitmap.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

                Parallel.For(0, heightInPixels, y =>
                {
                    byte* currentLine = ptrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int value = pixels[x / bytesPerPixel, y];
                        currentLine[x] = (byte)(value & 0x000000FF);
                        currentLine[x + 1] = (byte)((value & 0x0000FF00) >> 8);
                        currentLine[x + 2] = (byte)((value & 0x00FF0000) >> 16);
                        currentLine[x + 3] = 255;
                    }
                });                
                
                bitmap.UnlockBits(bitmapData);
            }
            return bitmap;
        }

        public void CopyToBitmap(Bitmap bitmap)
        {
            unsafe
            {
                BitmapData bitmapData = bitmap.LockBits(
                    new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                    ImageLockMode.ReadWrite, bitmap.PixelFormat);
                try
                {
                    int bytesPerPixel = Bitmap.GetPixelFormatSize(bitmap.PixelFormat) / 8;
                    int heightInPixels = bitmapData.Height;
                    int widthInBytes = bitmapData.Width * bytesPerPixel;
                    byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

                    //for (int y = 0; y < heightInPixels; ++y)
                    Parallel.For(0, heightInPixels, y =>
                    {
                        byte* currentLine = ptrFirstPixel + (y * bitmapData.Stride);

                        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                        {
                            int value = pixels[x / bytesPerPixel, y];
                            currentLine[x + 0] = (byte)(value & 0x000000FF);
                            currentLine[x + 1] = (byte)((value & 0x0000FF00) >> 8);
                            currentLine[x + 2] = (byte)((value & 0x00FF0000) >> 16);
                            currentLine[x + 3] = 255;
                        }
                    });
                }
                finally
                {
                    bitmap.UnlockBits(bitmapData);
                }
            }            
        }

        public void Clear(int value = 0)
        {
            int maxY = 8 * (height / 8);
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < maxY; y += 8)
                {
                    pixels[x, y] = value;
                    pixels[x, y + 1] = value;
                    pixels[x, y + 2] = value;
                    pixels[x, y + 3] = value;
                    pixels[x, y + 4] = value;
                    pixels[x, y + 5] = value;
                    pixels[x, y + 6] = value;
                    pixels[x, y + 7] = value;
                }

                for (int y = maxY; y < height; ++y)
                    pixels[x, y] = value;
            });         
        }

        public void Clear(Color color)
        {
            Clear(color.ToArgb());
        }
    }

    class ZBuffer
    {
        private int width, height;
        private double[,] pixels;

        public ZBuffer(int width, int height)
        {
            this.width = width;
            this.height = height;
            pixels = new double[width, height];

            for (int x = 0; x < width; ++x)
                for (int y = 0; y < height; ++y)
                    pixels[x, y] = double.MinValue;
        }

        public int Width
        {
            get
            {
                return width;
            }
        }

        public int Height
        {
            get
            {
                return height;
            }
        }

        private bool ValidIndex(int x, int y)
        {
            return (x >= 0) && (x < width) && (y >= 0) && (y < height);
        }

        public double this[int x, int y]
        {
            get
            {
                return ValidIndex(x, y) ? pixels[x, y] : 0;
            }
            set
            {
                if (ValidIndex(x, y))
                    pixels[x, y] = value;
            }
        }

        public double GetUnsafe(int x, int y)
        {
            return pixels[x, y];
        }

        public void SetUnsafe(int x, int y, double value)
        {
            pixels[x, y] = value;
        }

        public void Clear(double value = double.MinValue)
        {
            int maxY = 8 * (height / 8);
            Parallel.For(0, width, x =>
            {
                for (int y = 0; y < maxY; y += 8)
                {
                    pixels[x, y] = value;
                    pixels[x, y + 1] = value;
                    pixels[x, y + 2] = value;
                    pixels[x, y + 3] = value;
                    pixels[x, y + 4] = value;
                    pixels[x, y + 5] = value;
                    pixels[x, y + 6] = value;
                    pixels[x, y + 7] = value;
                }

                for (int y = maxY; y < height; ++y)
                    pixels[x, y] = value;
            });         
        }
    }
}
