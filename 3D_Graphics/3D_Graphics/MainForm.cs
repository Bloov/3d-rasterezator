﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Termpaper_3D_Graphics
{
    public partial class MainForm : Form
    {
        private bool initialized = false;
        private int drawMode;
        private bool rotateByMouse, distanceByMouse, rotateModel;
        private int mouseX, mouseY;
        private int oldMouseX, oldMouseY;

        private Render render;        
        private Model model;        

        public MainForm()
        {
            InitializeComponent();
            initialized = true;

            mouseX = mouseY = oldMouseX = oldMouseY = 0;
            rotateByMouse = false;
            distanceByMouse = false;
            rotateModel = false;
            
            render = new Render(Color.Black, Color.Yellow);
            btnWireColor.BackColor = Color.Yellow;
            btnBackColor.BackColor = Color.Black;
            render.SetRenderTargetSize(pbRender.ClientSize.Width, pbRender.ClientSize.Height);
            render.SetModelTransform(Transformation.Identity());
            render.SetViewTransform(Transformation.Identity());
            render.SetProjectionTransform(Transformation.Identity());
            pbRender.Image = render.GetRenderTarget();
            render.LightColor = Color.White;
            btnLightColor.BackColor = render.LightColor;
        
            btnBox_Click(null, null);
            Redraw();                       
        }
        
        private void Redraw()
        {
            switch (drawMode)
            {
                case 1: 
                    btnRedrawWire_Click(null, null);
                    break;

                case 2: 
                    btnRedrawFace_Click(null, null);
                    break;

                default: 
                    btnRedrawWire_Click(null, null);
                    break;
            }            
        }

        private double GetTBValue(TextBox tb, double defValue = 0f)
        {
            double result = 0;
            if (!double.TryParse(tb.Text, out result))
                result = defValue;

            return result;
        }

        private void btnBox_Click(object sender, EventArgs e)
        {
            model = ModelMaker.MakeBox(GetTBValue(tbModelSize, 180));
            Redraw();
        }

        private void btnModel_Click(object sender, EventArgs e)
        {
            model = ModelMaker.MakeCylinder(trbTessFactor.Value, 
                GetTBValue(tbOuterRadius, 60), GetTBValue(tbInnerRadius, 10), 
                GetHolePosition(), GetTBValue(tbModelSize, 120));
            Redraw();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (openModelDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var modelFile = openModelDialog.FileName;
                model = ModelMaker.LoadModel(modelFile, GetTBValue(tbModelSize, 500));
                Redraw();
            }
        }

        private Matrix4x4 GetModelTransform()
        {
            var dx = GetTBValue(tbDX, 0);
            var dy = GetTBValue(tbDY, 0);
            var dz = GetTBValue(tbDZ, 0);

            var sx = GetTBValue(tbSX, 1);
            var sy = GetTBValue(tbSY, 1);
            var sz = GetTBValue(tbSZ, 1);

            var rx = GetTBValue(tbRX, 0);
            var ry = GetTBValue(tbRY, 0);
            var rz = GetTBValue(tbRZ, 0);

            var result = Transformation.Identity();
            result = result.Multiply(Transformation.Scale(new Vector3(sx, sy, sz)));
            result = result.Multiply(Transformation.RotateX(rx * Math.PI / 180.0));
            result = result.Multiply(Transformation.RotateY(ry * Math.PI / 180.0));
            result = result.Multiply(Transformation.RotateZ(rz * Math.PI / 180.0));
            result = result.Multiply(Transformation.Shift(new Vector3(dx, dy, dz)));

            return result;
        }

        private Matrix4x4 GetViewPointTransform()
        {
            if (cbApplyVPT.Checked)
            {
                var ro = GetTBValue(tbVPRo, 100);
                var phi = GetTBValue(tbVPPh, 0) * Math.PI / 180.0;
                var theta = GetTBValue(tbVPTh, 0) * Math.PI / 180.0;

                //return ViewTransformation.ViewTransform(ro, phi, theta);
                return ViewTransformation.BuildViewMatrix(new Vector3(0, 0, 0), phi, theta, ro);
            }
            else
                return Transformation.Identity();
        }

        private void btnRedrawWire_Click(object sender, EventArgs e)
        {
            drawMode = 1;

            render.SetModelTransform(GetModelTransform());
            render.SetViewTransform(GetViewPointTransform());
            render.DoWireRender(model);
            pbRender.Image = render.GetRenderTarget();

            lblRenderTime.Text = string.Format("Render time: {0:F0}", render.RenderTime);
            lblBufferTime.Text = string.Format("Buffer time: {0:F0}", render.BufferTime);
        }        
        
        private void btnRedrawFace_Click(object sender, EventArgs e)
        {
            drawMode = 2;

            render.UseZBuffer = cbUseZBuffer.Checked;
            render.DrawZBuffer = cbDrawZ.Checked;
            render.DrawLight = cbLight.Checked;
            render.UseFlatLightning = cbUseFlatLightning.Checked;
            ApplyLightParameters();

            render.SetModelTransform(GetModelTransform());
            render.SetViewTransform(GetViewPointTransform());
            render.DoFaceRender(model);
            pbRender.Image = render.GetRenderTarget();

            lblRenderTime.Text = string.Format("Render time: {0:F0}", render.RenderTime);
            lblBufferTime.Text = string.Format("Buffer time: {0:F0}", render.BufferTime);
        }

        private Vector3 GetLightPosition()
        {
            var valueX = GetTBValue(tbLightX, 1000);
            tbLightX.Text = string.Format("{0:F2}", valueX);

            var valueY = GetTBValue(tbLightY, 1000);
            tbLightY.Text = string.Format("{0:F2}", valueY);

            var valueZ = GetTBValue(tbLightZ, 1000);
            tbLightZ.Text = string.Format("{0:F2}", valueZ);

            return new Vector3(valueX, valueY, valueZ);
        }

        private void ApplyLightParameters()
        {
            var kAmbient = GetTBValue(tbModelAmbient, 1);
            var kDiffuse = GetTBValue(tbModelDiffuse, 1);
            var kSpecular = GetTBValue(tbModelSpecular, 1);
            var kSpecularShines = GetTBValue(tbModelSpecularShines, 3);
            tbModelAmbient.Text = string.Format("{0:F2}", kAmbient);
            tbModelDiffuse.Text = string.Format("{0:F2}", kDiffuse);
            tbModelSpecular.Text = string.Format("{0:F2}", kSpecular);
            tbModelSpecularShines.Text = string.Format("{0:F2}", kSpecularShines);

            model.SetMaterial(kAmbient, kDiffuse, kSpecular, kSpecularShines);

            var iAmbient = GetTBValue(tbLightAmbient, 0.3);
            var iDiffuse = GetTBValue(tbLightDiffuse, 1.2);
            var iSpecular = GetTBValue(tbLightSpecular, 0.8);
            tbLightAmbient.Text = string.Format("{0:F2}", iAmbient);
            tbLightDiffuse.Text = string.Format("{0:F2}", iDiffuse);
            tbLightSpecular.Text = string.Format("{0:F2}", iSpecular);

            render.AmbientIntensy = iAmbient;
            render.DiffuseIntensy = iDiffuse;
            render.SpecularIntensy = iSpecular;
            render.LightPosition = GetLightPosition();
        }

        private void pbRender_ClientSizeChanged(object sender, EventArgs e)
        {
            if (!initialized)
                return;

            if ((pbRender.ClientSize.Width == 0) || (pbRender.ClientSize.Height == 0))
                return;

            var oldFlag = render.EyeByDirection;
            var oldValueA = render.EyeDirection;
            var oldValueB = render.EyePosition;

            var oldTransformA = render.ModelTransformation;
            var oldTransformB = render.ViewTransformation;
            var oldTransformC = render.ProjectionTransformation;

            var oldWireColor = render.WireColor;
            var oldBackColor = render.BackColor;
            var oldLightColor = render.LightColor;

            render = new Render(oldBackColor, oldWireColor);
            btnBackColor.BackColor = oldBackColor;
            render.SetModelTransform(oldTransformA);
            render.SetViewTransform(oldTransformB);
            render.SetProjectionTransform(oldTransformC);
            render.EyeByDirection = oldFlag;
            render.EyeDirection = oldValueA;
            render.EyePosition = oldValueB;
            render.LightColor = oldLightColor;
            btnLightColor.BackColor = oldLightColor;

            render.SetRenderTargetSize(pbRender.ClientSize.Width, pbRender.ClientSize.Height);
            pbRender.Image = render.GetRenderTarget();
            
            Redraw();
        }

        private void trbHolePosition_ValueChanged(object sender, EventArgs e)
        {
            lblHolePosition.Text = string.Format("{0:F2}", GetHolePosition());
            btnModel_Click(sender, e);
            Redraw();
        }

        private double GetHolePosition()
        { 
            var outer = GetTBValue(tbOuterRadius, 60);
            var inner = GetTBValue(tbInnerRadius, 10);
            return (outer * 0.95 - inner) * trbHolePosition.Value / (double)trbHolePosition.Maximum;
        }

        private void trbTessFactor_ValueChanged(object sender, EventArgs e)
        {
            lblTessValue.Text = trbTessFactor.Value.ToString();
            btnModel_Click(sender, e);
            Redraw();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            tbDX.Text = "0";
            tbDY.Text = "0";
            tbDZ.Text = "0";
            tbSX.Text = "1";
            tbSY.Text = "1";
            tbSZ.Text = "1";
            tbRX.Text = "0";
            tbRY.Text = "0";
            tbRZ.Text = "0";
            Redraw();
        }

        private void btnApplyTransform_Click(object sender, EventArgs e)
        {
            Redraw();
        }

        private void btnFrontalView_Click(object sender, EventArgs e)
        {            
            render.SetProjectionTransform(ViewTransformation.Frontal());
            render.EyeByDirection = true;
            render.EyeDirection = new Vector3(0, 0, 1);
            Redraw();
        }

        private void btnHorizontalView_Click(object sender, EventArgs e)
        {
            var projectionTransform = ViewTransformation.Horizontal();
            projectionTransform = projectionTransform.Multiply(ViewTransformation.ExchangeYZ());
            render.SetProjectionTransform(projectionTransform);
            render.EyeByDirection = true;
            render.EyeDirection = new Vector3(0, 1, 0);
            Redraw();
        }

        private void btnProfileView_Click(object sender, EventArgs e)
        {
            var projectionTransform = ViewTransformation.Profile();
            projectionTransform = projectionTransform.Multiply(ViewTransformation.ExchangeXY());
            projectionTransform = projectionTransform.Multiply(ViewTransformation.ExchangeYZ());
            render.SetProjectionTransform(projectionTransform);
            render.EyeByDirection = true;
            render.EyeDirection = new Vector3(1, 0, 0);
            Redraw();
        }

        private void btnOblique_Click(object sender, EventArgs e)
        {
            var l = GetTBValue(tbObliqueL, 0.6);
            var angle = GetTBValue(tbObliqueAngle, 45);            
            render.SetProjectionTransform(ViewTransformation.Oblique(l, angle));
            render.EyeByDirection = true;
            render.EyeDirection = new Vector3(0, 0, 1);
            Redraw();
        }

        private void btnAxonometric_Click(object sender, EventArgs e)
        {
            var phi = GetTBValue(tbAxonometrixPhi, 45) * Math.PI / 180.0;
            var ksi = GetTBValue(tbAxonometrixKsi, 0) * Math.PI / 180.0;            
            render.SetProjectionTransform(ViewTransformation.Axonometric(phi, ksi));
            render.EyeByDirection = true;
            render.EyeDirection = new Vector3(0, 0, 1);
            Redraw();
        }

        private void btnPerspective_Click(object sender, EventArgs e)
        {
            var d = GetTBValue(tbPerspectiveD, 10);            
            render.SetProjectionTransform(ViewTransformation.Perspective(d));
            cbApplyVPT.Checked = true;
            render.EyeByDirection = false;
            render.EyePosition = new Vector3(0, 0, 0);
            Redraw();
        }

        private void btnFrustum_Click(object sender, EventArgs e)
        {
            var fovy = GetTBValue(tbFovY, 0.1) * Math.PI / 180.0;
            var zFar = GetTBValue(tbZFar, -5000);
            var zNear = GetTBValue(tbZNear, 5000);            
            render.SetProjectionTransform(ViewTransformation.Frustum(fovy, 1, zFar, zNear));
            cbApplyVPT.Checked = true;
            render.EyeByDirection = false;
            render.EyePosition = new Vector3(0, 0, 0);
            Redraw();
        }

        private double NormalizeAngle(double angle)
        {
            while (angle > 360)
                angle -= 360;
            while (angle < 0)
                angle += 360;

            return angle;
        }

        private void btnRXAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbRX, 0) + 10;
            value = NormalizeAngle(value);
            tbRX.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnRXSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbRX, 0) - 10;
            value = NormalizeAngle(value);
            tbRX.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnRYAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbRY, 0) + 10;
            value = NormalizeAngle(value);
            tbRY.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnRYSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbRY, 0) - 10;
            value = NormalizeAngle(value);
            tbRY.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnRZAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbRZ, 0) + 10;
            value = NormalizeAngle(value);
            tbRZ.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnRZSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbRZ, 0) - 10;
            value = NormalizeAngle(value);
            tbRZ.Text = string.Format("{0:F2}", value);
            Redraw();
        }        

        private void btnVPThAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbVPTh, 0) + 10;
            value = NormalizeAngle(value);
            tbVPTh.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnVPThSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbVPTh, 0) - 10;
            value = NormalizeAngle(value);
            tbVPTh.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnVPPhAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbVPPh, 0) + 10;
            value = NormalizeAngle(value);
            tbVPPh.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnVPPhSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbVPPh, 0) - 10;
            value = NormalizeAngle(value);
            tbVPPh.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void cbApplyVPT_CheckedChanged(object sender, EventArgs e)
        {
            Redraw();
        }        

        private void btnVPRoAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbVPRo, 1200) + 200;
            tbVPRo.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnVPRoSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbVPRo, 1200) - 200;
            if (value < 0)
                value = 0;
            tbVPRo.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnDXAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbDX, 0) + 50;
            tbDX.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnDXSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbDX, 0) - 50;
            tbDX.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnDYAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbDY, 0) + 50;
            tbDY.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnDYSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbDY, 0) - 50;
            tbDY.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnDZAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbDZ, 0) + 50;
            tbDZ.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnDZSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbDZ, 0) - 50;
            tbDZ.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnSXAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbSX, 0) * 1.1;
            tbSX.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnSXSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbSX, 0) * 0.9;
            tbSX.Text = string.Format("{0:F2}", value);
            Redraw();                   
        }

        private void btnSYAdd_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbSY, 0) * 1.1;
            tbSY.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnSYSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbSY, 0) * 0.9;
            tbSY.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void f_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbSZ, 0) * 1.1;
            tbSZ.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void btnSZSub_Click(object sender, EventArgs e)
        {
            var value = GetTBValue(tbSZ, 0) * 0.9;
            tbSZ.Text = string.Format("{0:F2}", value);
            Redraw();
        }

        private void cbDrawZ_CheckedChanged(object sender, EventArgs e)
        {
            Redraw();
        }

        private void cbUseFlatLightning_CheckedChanged(object sender, EventArgs e)
        {
            Redraw();
        }

        private void cbUseZBuffer_CheckedChanged(object sender, EventArgs e)
        {
            Redraw();
        }

        private void pbRender_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                rotateByMouse = true;
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
                distanceByMouse = true;
            if (e.Button == System.Windows.Forms.MouseButtons.Middle)
                rotateModel = !rotateModel;
        }

        private void pbRender_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                rotateByMouse = false;
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
                distanceByMouse = false;            
        }

        private void pbRender_MouseMove(object sender, MouseEventArgs e)
        {
            oldMouseX = mouseX;
            oldMouseY = mouseY;
            mouseX = e.X;
            mouseY = e.Y;
            if (rotateByMouse)
            {
                if (rotateModel)
                {
                    var valueX = GetTBValue(tbRX, 0);
                    valueX = valueX + (mouseX - oldMouseX) * 0.2;
                    valueX = NormalizeAngle(valueX);

                    var valueY = GetTBValue(tbRY, 0);
                    valueY = valueY + (mouseY - oldMouseY) * 0.2;
                    valueY = NormalizeAngle(valueY);

                    tbRX.Text = string.Format("{0:F2}", valueX);
                    tbRY.Text = string.Format("{0:F2}", valueY);
                }
                else
                {
                    var valueX = GetTBValue(tbVPTh, 0);
                    valueX = valueX + (mouseX - oldMouseX) * 0.2;
                    valueX = NormalizeAngle(valueX);

                    var valueY = GetTBValue(tbVPPh, 0);
                    valueY = valueY + (mouseY - oldMouseY) * 0.2;
                    valueY = NormalizeAngle(valueY);

                    tbVPTh.Text = string.Format("{0:F2}", valueX);
                    tbVPPh.Text = string.Format("{0:F2}", valueY);
                }
            }

            if (distanceByMouse)
            {
                var value = GetTBValue(tbVPRo, 0);
                value = value + (mouseY - oldMouseY) * 5;
                if (value < 0)
                    value = 0;

                tbVPRo.Text = string.Format("{0:F2}", value);  
            }

            if (rotateByMouse || distanceByMouse)
                Redraw();  
        }

        private void cbLight_CheckedChanged(object sender, EventArgs e)
        {
            Redraw();
        }

        private void btnApplyLight_Click(object sender, EventArgs e)
        {
            Redraw();
        }

        private void btnResetLight_Click(object sender, EventArgs e)
        {
            tbLightX.Text = "0";
            tbLightY.Text = "0";
            tbLightZ.Text = "1000";

            tbLightAmbient.Text = "0,25";
            tbLightDiffuse.Text = "0.7";
            tbLightSpecular.Text = "1.2";

            tbModelAmbient.Text = "1";
            tbModelDiffuse.Text = "1";
            tbModelSpecular.Text = "1";

            Redraw();
        }

        private void btnLightColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                btnLightColor.BackColor = colorDialog.Color;
                render.LightColor = colorDialog.Color;
                Redraw();
            }
        }

        private void btnBackColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                btnBackColor.BackColor = colorDialog.Color;
                render.BackColor = colorDialog.Color;
                Redraw();
            }
        }

        private void btnWireColor_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                btnWireColor.BackColor = colorDialog.Color;
                render.WireColor = colorDialog.Color;
                Redraw();
            }
        }        
    }
}
